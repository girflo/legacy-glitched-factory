# Readme
## About
This project contain the code for compiling the GlitchedFactory as well as the current version of the binaries (windows, linux and osx). To get older version you can find them in the [repository of the website](https://framagit.org/Radoteur/glitchedfactory-website/-/tree/master/bin).
## Specs
### Initialization
You need Rspec to run the specs for the project, to install it run `bundle install` in the root folder.

### Running
To run the specs simply run the following command `bundle exec rspec`

## Compile the project
### Linux
#### .deb package
For creating a .deb package you need to copy `script/utils/merge-lin.rb` inside `~/.shoes/package/` on the linux setup then run the following commands from the root folder of glitched factory:
```
scp script/utils/linux_packager.sh user@host:linux_packager.sh
ssh user@host:
./linux_packager.sh
```
### OSX
```
cd script/
mkdir dmg
cd dmg
ruby ../utils/dmg_packager_osx.rb ../utils/dmg_info_osx.yml
```
### Window
Copy `script/utils/compile_glitched_factory.bat` to the vm and run it as administrator
## Create new release
```
cd script
./create_new_version.sh <version number>
```

