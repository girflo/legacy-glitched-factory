module ProfileAccessor
  require 'json'

  def self.check_name_existence(name, profile_key)
    name_already_used = false
    parsed_profile_file = ProfileAccessor.parse_profile_file
    parsed_profile_file[profile_key].each do |profile|
      name_already_used = true if profile['name'] == name
    end
    name_already_used
  end

  def self.parse_json_file(json_file)
    file = File.open(json_file)
    json = file.read
    parsed = JSON.parse(json)
    parsed
  end

  def self.parse_profile_file
    ProfileAccessor.parse_json_file('static/profile.json')
  end

  def self.get_profile_content(profile_name, profile_key)
    matching_profiles = ProfileAccessor.all_profiles(profile_key).select { |pro| pro['name'] == profile_name }
    matching_profiles.empty? ? {} : matching_profiles.first['content']
  end

  def self.all_profiles(profile_key)
    current_profiles = ProfileAccessor.parse_profile_file
    current_profiles.key?(profile_key) ? current_profiles[profile_key] : []
  end

  def self.save_to_profile_file(content)
    File.open('static/profile.json', 'w') do |f|
      f.puts JSON.pretty_generate(content)
    end
  end

  def self.generate_content(library, metadata, options)
    { metadata: metadata }.merge(library: library).merge(options)
  end

  def self.create_new_profile(name, library, metadata, options)
    content = ProfileAccessor.generate_content(library, metadata, options)
    hash = { name: name, content: content }
    current_profiles = ProfileAccessor.parse_profile_file
    current_profiles['profiles_glitcher'] << hash
    ProfileAccessor.save_to_profile_file(current_profiles)
  end

  def self.create_new_sorter_profile(name, metadata, options)
    content = { metadata: metadata }.merge(options)
    hash = { name: name, content: content }
    current_profiles = ProfileAccessor.parse_profile_file
    current_profiles['profiles_sorter'] << hash
    ProfileAccessor.save_to_profile_file(current_profiles)
  end

  def self.import_profile(type, profile)
    current_profiles_file = ProfileAccessor.parse_profile_file
    current_profiles = current_profiles_file["profiles_#{type}"]
    already_create_profile = current_profiles.select { |pro| pro['name'] == profile['name'] }
    unless already_create_profile.empty?
      profile['name'] = "#{profile['name']}_#{Time.now.getutc}"
    end
    current_profiles << profile
    ProfileAccessor.save_to_profile_file(current_profiles_file)
  end

  def self.import_profiles_from_file(file)
    json = ProfileAccessor.parse_json_file(file)
    glitcher_profiles = json['profiles_glitcher']
    glitcher_profiles.each { |pro| ProfileAccessor.import_profile('glitcher', pro) }
    sorter_profiles = json['profiles_sorter']
    sorter_profiles.each { |pro| ProfileAccessor.import_profile('sorter', pro) }
  end
end
