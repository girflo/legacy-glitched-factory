require 'yaml'

class SettingUpdater

  def initialize(params = {})
    @tips_visibility = validate_boolean(params[:tips_visibility]) unless params[:tips_visibility].nil?
    @queue_autoreload = validate_boolean(params[:queue_autoreload]) unless params[:queue_autoreload].nil?
    @compact_interface = validate_boolean(params[:compact_interface]) unless params[:compact_interface].nil?
    @algorithm_in_name = validate_boolean(params[:algorithm_in_name]) unless params[:algorithm_in_name].nil?
    @default_input = params[:default_input] || ''
    @default_input_type = params[:default_input_type] || ''
    @default_output = params[:default_output] || ''
    @default_input_sampler = params[:default_input_sampler] || ''
    @default_input_sorter = params[:default_input_sorter] || ''
  end

  def call
    settings = { 'tips_visible' => @tips_visibility,
                 'queue_autoreload' => @queue_autoreload,
                 'compact_interface' => @compact_interface,
                 'algorithm_in_name' => @algorithm_in_name,
                 'default_input' => @default_input,
                 'default_input_type' => @default_input_type,
                 'default_output' => @default_output,
                 'default_input_sampler' => @default_input_sampler,
                 'default_input_sorter' => @default_input_sorter }
    update_settings_file(settings)
  end

  private

  def update_settings_file(settings_hash)
    setting_yaml_file = YAML.safe_load(settings_content) || {}
    settings_hash.each do |k, v|
      setting_yaml_file[k] = v
    end
    File.open(settings_file_location, 'w') { |file| YAML.dump(setting_yaml_file, file) }
  end

  def settings_file_location
    'settings.yml'
  end

  def settings_content
    File.open(settings_file_location).read
  end

  def validate_boolean(value)
    !!value == value ? value : true
  end
end
