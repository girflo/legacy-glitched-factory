require 'ffi'

module RustyEngine
  extend FFI::Library

  lib_name = 'librusty_engine.' + FFI::Platform::LIBSUFFIX
  ffi_lib File.expand_path(lib_name, __dir__)
  # Params : input output
  attach_function :convert, [:string, :string], :void, { blocking: true }

  # Params : input output proba min max direction
  # Directions : 1 -> horizontal
  #              2 -> vertical
  #              3 -> horizontal_inverted
  #              4 -> vertical_inverted
  attach_function :brush, [:string, :string, :string, :string, :string, :string], :void, { blocking: true }

  # Params : input, output, proba, probability_area, direction, colors, color_with_proba
  # Directions : 1 -> up_to_down
  #              2 -> down_to_up
  #              3 -> left_to_right
  #              4 -> right_to_left
  attach_function :slim, [:string, :string, :string, :string, :string, :string, :string], :void, { blocking: true }
end
