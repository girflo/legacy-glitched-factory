class Io
  attr_reader :input_type, :input_path, :output_folder
  attr_accessor :output_filename

  def initialize(io_params)
    if io_params[:input_file].nil? || io_params[:input_file].empty?
      @input_path = io_params[:input_folder]
      @input_type = 'directory'
    else
      @input_path = io_params[:input_file]
      @input_type = 'image'
    end
    @output_folder = io_params[:output_folder] || 'glitched'
    @output_filename = io_params[:output_filename] || 'glitch'
    Dir.mkdir @output_folder unless File.directory?(@output_folder)
  end

  def images
    if @input_type == 'directory'
      images = png_images_in_directory.map do |img|
        { input_filename: "#{@input_path}/#{img}",
          output_filename: define_output_filename_in_directory(img) }
      end
    else
      images = [{ input_filename: @input_path,
                  output_filename: define_output_filename }]
    end
    images
  end

  def jpg_images
    if @input_type == 'directory'
      images = jpg_images_in_directory.map do |img|
        { input_filename: "#{@input_path}/#{img}",
          output_filename: define_jpg_output_filename(img) }
      end
    else
      images = [{ input_filename: @input_path,
                  output_filename: define_jpg_output_filename(@input_path) }]
      "#{@output_folder}/#{@output_filename}.png"
    end
    images
  end

  def define_output_filename_in_directory(img)
    if File.file?("#{@output_folder}/#{@output_filename}#{img}")
      "#{@output_folder}/#{@output_filename}#{time_utc_string}_#{img}"
    else
      "#{@output_folder}/#{@output_filename}#{img}"
    end
  end

  def define_jpg_output_filename(img)
    png_img = "#{File.basename(img, File.extname(img))}.png"
    if File.file?("#{@output_folder}/#{png_img}")
      "#{@output_folder}/#{time_utc_string}_#{png_img}"
    else
      "#{@output_folder}/#{png_img}"
    end
  end

  def define_output_filename
    if File.file?("#{@output_folder}/#{@output_filename}.png")
      "#{@output_folder}/#{time_utc_string}_#{@output_filename}.png"
    else
      "#{@output_folder}/#{@output_filename}.png"
    end
  end

  def time_utc_string
    t = Time.now.getutc
    t.strftime "%Y%m%d_%H%M%S"
  end

  def png_images_in_directory
    png_children = []
    Dir.foreach(@input_path) do |item|
      next unless item.end_with?('.png') || item.end_with?('.PNG')

      png_children << item
    end
    png_children
  end

  def jpg_images_in_directory
    jpg_children = []
    Dir.foreach(@input_path) do |item|
      next unless ['.jpg', '.jpeg', '.JPG', '.JPEG'].include? File.extname(item)

      jpg_children << item
    end
    jpg_children
  end
end
