module JobsQueue
  class JobCreator
    require_relative 'glitchers/glitcher.rb'
    require_relative 'glitchers/sampler.rb'
    require_relative 'glitchers/sorter.rb'
    require_relative 'converter.rb'
    require_relative 'io.rb'

    def initialize(job_kind, name, io_params, metadata, options)
      @io = Io.new(io_params)
      @metadata = metadata
      @options = options
      @name = name
      @type = @io.input_type
      @action = define_action(job_kind)
    end

    def define_action(job_kind)
      if job_kind == :glitcher
        Glitcher.new(@name, @io, false, @metadata, @options)
      elsif job_kind == :sorter
        Sorter.new(@io, @metadata, @options)
      elsif job_kind == :sampler
        Sampler.new(@io, @metadata, @options)
      elsif job_kind == :converter
        Converter.new(@io)
      end
    end

    def call
      @@jobs_queue.add_job(@name, @type, @action)
    end
  end

  class Job
    attr_reader :name, :type, :state

    def initialize(name, type, action)
      @name = name
      @type = type
      @state = :pending
      @action = action
    end

    def execute
      start
      @action.call
    end

    def start
      @state = :running
    end

    def running?
      @state == :running
    end
  end

  class Manager
    require 'concurrent'
    include Concurrent::Async
    attr_reader :jobs

    def initialize
      super()
      @jobs = Array.new
      @threads = []
      @state = :waiting
    end

    def add_job(name, type, action)
      job = Job.new(name, type, action)
      @jobs << job
      self.async.run_queue_entirely
    end

    def remove_job(job)
      new_jobs_array = @jobs.reject { |j| j == job }
      @jobs = new_jobs_array
    end

    def run_queue_entirely
      execute until @jobs.empty?
    end

    def execute
      return true if running?

      tasks = @jobs.clone
      @state = :running
      until tasks.empty?
        current_job = tasks.shift
        @jobs.first.start
        current_job.execute if current_job
        @jobs.shift
      end
      @state = :waiting
    end

    def running?
      @state == :running
    end

    # def close_queue
    #   success_message = CONTENT["success_single"]
    #   systray title: CONTENT["app_title"], message: success_message, icon: 'static/images/logo.png'
    # end
  end
end
