require_relative 'rusty_engine/rusty_engine.rb'
class Converter
  def initialize(io)
    @io = io
  end

  def call
    @io.jpg_images.each do |jpg|
      RustyEngine.convert(jpg[:input_filename], jpg[:output_filename])
    end
  end
end
