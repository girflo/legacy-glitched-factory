class Sampler
  require_relative 'glitcher.rb'
  require_relative '../io.rb'

  FILTERS = %w(none sub up average paeth)
  SLIM_DIRECTION = %w(up_to_down down_to_up left_to_right right_to_left)
  BRUSH_DIRECTION = %w(vertical vertical_inverted horizontal horizontal_inverted)

  def initialize(io, metadata, options)
    @io = io
    @metadata = metadata
    @base_output_filename = options[:base_output_filename] || 'sample'
    @algorithms_to_sample = options[:algorithms_to_sample]
  end

  def call
    FILTERS.each do |filter|
      sample_exchange(filter)
      sample_transpose(filter)
      sample_wrong_filter(filter)
    end
    sample_slim
    sample_brush
    sample_change_byte
    sample_defect
  end

  private

  def update_io(output_filename_appendix)
    @io.output_filename = "#{@base_output_filename }_#{output_filename_appendix}"
  end

  def sample_exchange(filter)
    return unless @algorithms_to_sample.include? 'exchange'

    exchange_options = { filter: filter }
    update_io("exchange_#{filter}")
    Glitcher.new('Exchange', @io, true, @metadata, exchange_options).call
  end

  def sample_transpose(filter)
    return unless @algorithms_to_sample.include? 'transpose'

    transpose_options = { filter: filter }
    update_io("transpose_#{filter}")
    Glitcher.new('Transpose', @io, true, @metadata, transpose_options).call
  end

  def sample_wrong_filter(filter)
    return unless @algorithms_to_sample.include? 'wrong_filter'

    wrong_filter_options = { algorithm: 'wrong_filter',
                              filter: filter }
    update_io("wrong_filter_#{filter}")
    Glitcher.new('WrongFilter', @io, true, @metadata, wrong_filter_options).call
  end

  def sample_slim
    return unless @algorithms_to_sample.include? 'slim'

    SLIM_DIRECTION.each do |direction|
      update_io("slim_#{direction}")
      slim_options = { direction: direction.to_sym }
      Glitcher.new('Slim', @io, true, @metadata, slim_options).call
    end
  end

  def sample_brush
    return unless @algorithms_to_sample.include? 'brush'

    BRUSH_DIRECTION.each do |direction|
      update_io("brush_#{direction}")
      brush_options = { direction: direction.to_sym }
      Glitcher.new('Brush', @io, true, @metadata, brush_options).call
    end
  end

  def sample_change_byte
    return unless @algorithms_to_sample.include? 'change_byte'

    update_io("change_byte")
    change_byte_options = { algorithm: 'change_byte' }
    Glitcher.new('ChangeByte', @io, true, @metadata, change_byte_options).call
  end

  def sample_defect
    return unless @algorithms_to_sample.include? 'defect'

    update_io("defect")
    defect_options = { random: true, iterations: 10 }
    Glitcher.new('Defect', @io, true, @metadata, defect_options).call
  end
end
