class Sorter
  require_relative 'pixel_sorter.rb'
  require_relative 'metadata.rb'

  def initialize(io, metadata, options)
    @options = options
    @metadata = metadata
    @io = io
    @steps = define_steps
  end

  def call
    @io.png_images_in_directory.sort_by { |name| [name[/\d+/].to_i, name] }.each_with_index do |item, index|
      input_filename = "#{@io.input_path}/#{item}"
      output_filename = define_output_filename(item)

      PixelSorter.new(sorter_options(index)).call(input_filename, output_filename )
      Metadata.new(input_filename, output_filename, 'PixelSorter').call if @metadata
    end
  end

  private

  def define_output_filename(item)
    if File.file?("#{@io.output_folder}/#{@io.output_filename}#{item}")
      "#{@io.output_folder}/#{time_utc_string}_#{@io.output_filename}#{item}"
    else
      "#{@io.output_folder}/#{@io.output_filename}#{item}"
    end
  end

  def time_utc_string
    t = Time.now.getutc
    t.strftime "%Y%m%d_%H%M%S"
  end

  def sorter_options(index)
    return @options if !@options[:smart_sorting] 

    step_options = @options.clone
    step_options[:multiple_ranges] = false
    step_options[:detection_min] = define_new_min_limit(index)
    step_options[:detection_max] = define_new_max_limit(index)
    step_options
  end

  def define_steps
    images_number = @io.png_images_in_directory.count
    min_step = positive_difference(@options[:detection_min].to_f, @options[:detection_min_2].to_f) / (images_number - 1)
    max_step = positive_difference(@options[:detection_max].to_f, @options[:detection_max_2].to_f) / (images_number - 1)
    { min: min_step, max: max_step }
  end

  def positive_difference(value1, value2)
    if value1 > value2
      value1 - value2
    else
      value2 - value1
    end
  end

  def define_new_min_limit(index)
    if @options[:detection_min].to_f > @options[:detection_min_2].to_f
      (@options[:detection_min].to_f - (index * @steps[:min]))
    else
      (@options[:detection_min].to_f + (index * @steps[:min]))
    end
  end

  def define_new_max_limit(index)
    if @options[:detection_max].to_f > @options[:detection_max_2].to_f
      (@options[:detection_max].to_f - (index * @steps[:max]))
    else
      (@options[:detection_max].to_f + (index * @steps[:max]))
    end
  end
end
