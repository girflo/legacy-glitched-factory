require 'pnglitch'
class Defect
  def initialize(options = {})
    @random = options[:random] || false
    @iterations = options[:iterations] || 1
  end

  def call(input_image, output_image)
    PNGlitch.open(input_image) do |png|
      filtered_glitch(png).save output_image
    end
    output_image
  end

  private

  def filtered_glitch(png)
    png.glitch do |data|
      if !@random
        data.gsub(/\d/, '')
      else
        @iterations.times do
          data[rand(data.size)] = ''
        end
        data
      end
    end
  end
end
