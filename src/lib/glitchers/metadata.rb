require 'chunky_png'
class Metadata
  def initialize(input_filename, output_filename, content)
    @input_filename = input_filename
    @output_filename = output_filename
    @content = content
  end

  def call
    image = ChunkyPNG::Image.from_file(@output_filename)
    image.metadata['exif:Software'] = CONTENT["app_title"]
    image.metadata['exif:GlitchLibrary'] = metadata_description
    image.save(@output_filename)
  end

  private

  def metadata_description
    input_image = ChunkyPNG::Image.from_file(@input_filename)
    metadata = input_image.metadata['exif:GlitchLibrary']
    metadata == '' ? @content.to_s : "#{metadata} #{@content}"
  end
end
