class Glitcher
  require_relative 'brush.rb'
  require_relative 'exchange.rb'
  require_relative 'defect.rb'
  require_relative 'transpose.rb'
  require_relative 'wrong_filter.rb'
  require_relative 'change_byte.rb'
  require_relative 'metadata.rb'
  require_relative 'pixel_sorter.rb'
  require_relative 'slim.rb'
  require_relative '../io.rb'

  def initialize(glitch_library, io, sampler, metadata, options)
    @glitch_library = glitch_library
    @metadata = metadata
    @io = io
    @io.output_filename = define_output_filename_suffix(@io.output_filename,
                                                        sampler)
    @options = options || {}
  end

  def call
    @io.images.each do |item|
      create_glitch(item[:input_filename], item[:output_filename])
    end
  end

  private

  def define_output_filename_suffix(output_filename, sampler)
    if @@settings['algorithm_in_name'] && sampler == false
      "#{SanitizerHelper.underscore(@glitch_library)}_#{output_filename}"
    else
      output_filename
    end
  end

  def create_glitch(input_path, output_filename)
    Object.const_get(@glitch_library)
          .new(@options)
          .call(input_path, output_filename)
    Metadata.new(input_path, output_filename, @glitch_library).call if @metadata
    return true
  end
end
