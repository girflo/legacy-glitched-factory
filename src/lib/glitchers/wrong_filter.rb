require 'pnglitch'
class WrongFilter
  def initialize(options = {})
    @filter = options[:filter] || 'average'
  end

  def call(input_image, output_image)
    PNGlitch.open(input_image) do |png|
      filtered_glitch(png, @filter).save output_image
    end
    output_image
  end

  private

  def define_random_filter
    available_filters = %w(none sub up average paeth)
    available_filters[rand(5)]
  end

  def filtered_glitch(png, custom_filter)
    png.each_scanline do |scanline|
      if custom_filter == 'random'
        scanline.graft define_random_filter
      else
        scanline.graft custom_filter
      end
    end
  end
end
