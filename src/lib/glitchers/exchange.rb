require 'pnglitch'
class Exchange
  def initialize(options = {})
    @filter = define_filter(options[:filter]) || 'average'
    @random = options[:random] || false
    @range = options[:range] || 0
    @seed = options[:seed] || 'x'
  end

  def call(input_image, output_image)
    PNGlitch.open(input_image) do |png|
      filtered_glitch(png, @filter).save output_image
    end
    output_image
  end

  private

  def define_filter(filter_from_options)
    return filter_from_options unless filter_from_options == 'random'

    available_filters = %w(none sub up average paeth)
    available_filters[rand(5)]
  end

  def give_me_a_letter(index = rand(26))
    ('a'..'z').to_a[index]
  end

  def filtered_glitch(png, custom_filter)
    png.each_scanline do |scanline|
      scanline.change_filter custom_filter
    end
    png.glitch do |data|
      exchange_data(data)
    end
  end

  def exchange_data(data)
    if @range.zero?
      letter = @random ? give_me_a_letter.to_s : @seed
      data.gsub(/\d/, letter)
    else
      @range.times do
        data[rand(data.size)] = give_me_a_letter.to_s
      end
      data
    end
  end
end
