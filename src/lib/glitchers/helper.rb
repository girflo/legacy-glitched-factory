require 'chunky_png'

module Helper
  def self.hsl_to_color_group(pixel)
    hsl = ChunkyPNG::Color.to_hsl(pixel)
    return :white if hsl[2] > 0.90
    return :black if hsl[2] < 0.10
    return :grey if hsl[1] < 0.25
    return :red if hsl[0] < 30
    return :yellow if hsl[0] >= 30 && hsl[0] < 70
    return :green if hsl[0] >= 70 && hsl[0] < 150
    return :cyan if hsl[0] >= 150 && hsl[0] < 210
    return :blue if hsl[0] >= 210 && hsl[0] < 270
    return :magenta if hsl[0] >= 270 && hsl[0] < 350

    :red
  end
end
