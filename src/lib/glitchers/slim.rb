require_relative '../rusty_engine/rusty_engine.rb'

class Slim
  ALL_COLORS = %i(white black grey red green blue cyan yellow magenta).freeze
  def initialize(options = {})
    @colors = options[:colors] || ALL_COLORS
    @direction = options[:direction] || :up_to_down
    @probability = (options[:probability] || 95).to_s
    @probability_area = options[:probability_area] || 'global'
    @colors_with_proba = colors_with_proba(options[:colors_with_proba])
  end

  def call(input_image, output_image)
    RustyEngine.slim(input_image, output_image, @probability, @probability_area,
                     rust_formatted_direction, rust_formatted_colors,
                     rust_formatted_color_with_proba)
  end

  private

  def colors_with_proba(colors_with_proba_options)
    if colors_with_proba_options.nil? || colors_with_proba_options.empty?
      ALL_COLORS.map { |c| [c.to_sym, @probability] }.to_h
    else
      colors_with_proba_options
    end
  end

  def rust_formatted_direction
    ruby_to_rust_directions = { up_to_down: '1', down_to_up: '2',
                                left_to_right: '3', right_to_left: '4' }
    ruby_to_rust_directions[@direction]
  end

  def rust_formatted_colors
    @colors.join(',')
  end

  def rust_formatted_color_with_proba
    @colors_with_proba.map do |key, value|
      key.to_s + ':' + value.to_s
    end.join(',')
  end
end
