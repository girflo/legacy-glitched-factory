require 'chunky_png'
require_relative 'helper.rb'
class PixelSorter
  def initialize(options = {})
    @direction = options[:direction] || 'vertical'
    @smart_sorting = options[:smart_sorting]
    inverted = options[:inverted] || false
    @inverted = inverted ? -1 : 1
    @detection_type = options[:detection_type] || 'lightness_range'
    @detection_max = options[:detection_max] || '60'
    @detection_min = options[:detection_min] || '45'
    @multiple_ranges = options[:multiple_ranges] || false
    @detection_max_2 = options[:detection_max_2] || '90'
    @detection_min_2 = options[:detection_min_2] || '75'
    @sorting_by = options[:sorting_by] || 'hue'
  end

  def call(input_image, output_image)
    image = ChunkyPNG::Image.from_file(input_image)
    sorted_image = self.send "sort_#{@direction}".to_sym, image
    sorted_image.save(output_image, interlace: true)
    sorted_image
  end

  private

  def sort_horizontal(image)
    sorting_kind = @smart_sorting ? 'smart' : 'brut'
    (0..image.height - 1).each do |h|
      self.send "horizontal_#{sorting_kind}_sorting".to_sym, image, h
    end
    image
  end

  def sort_vertical(image)
    sorting_kind = @smart_sorting ? 'smart' : 'brut'
    (0..image.width - 1).each do |w|
      self.send "vertical_#{sorting_kind}_sorting".to_sym, image, w
    end
    image
  end

  def horizontal_smart_sorting(image, h)
    max_width = image.width - 1
    start = 0
    strip = [image[0,h]]
    (1..max_width).each do |w|
      previous_pixel = image[(w-1),h]
      current_pixel = image[w,h]
      if grouped_pixels?(previous_pixel, current_pixel) && w != max_width
        strip.push(image[w,h])
      else
        if strip.size > 1
          new_strip = self.send "sort_by_#{@sorting_by}".to_sym, strip
          (start..( w - 1)).each_with_index do |new_w, index|
            image[new_w, h] = new_strip[index]
          end
        end
        strip = [image[w, h]]
        start = w + 1
      end
    end
  end 

  def horizontal_brut_sorting(image, h)
    max_width = image.width - 1
    strip = (0..max_width).map { |w| image[w, h] }
    new_strip = self.send "sort_by_#{@sorting_by}".to_sym, strip
    (0..max_width).each do |w|
      image[w,h] = new_strip[w]
    end
  end

  def vertical_smart_sorting(image, w)
    max_height = image.height - 1
    start = 0
    strip = [image[w, 0]]
    (1..max_height).each do |h|
      previous_pixel = image[w, (h - 1)]
      current_pixel = image[w, h]
      if grouped_pixels?(previous_pixel, current_pixel) && h != max_height
        strip.push(image[w, h])
      else
        if strip.size > 1
          new_strip = self.send "sort_by_#{@sorting_by}".to_sym, strip
          (start..(h - 1)).each_with_index do |new_h, index|
            image[w, new_h] = new_strip[index]
          end
        end
        strip = [image[w, h]]
        start = h + 1
      end
    end
  end

  def grouped_pixels?(pixel1, pixel2)
    (pixel_group(pixel1, 1) != false &&
     pixel_group(pixel1, 1) == pixel_group(pixel2, 1)) ||
      (@multiple_ranges &&
       pixel_group(pixel1, 2) != false &&
       pixel_group(pixel1, 2) == pixel_group(pixel2, 2))
  end

  def vertical_brut_sorting(image, w)
    max_height = image.height - 1
    strip = (0..max_height).map { |h| image[w, h] }
    new_strip = self.send "sort_by_#{@sorting_by}".to_sym, strip
    (0..max_height).each do |h|
      image[w, h] = new_strip[h]
    end
  end

  def sort_by_hue(strip)
    strip.sort_by do |c|
      @inverted * pixel_hue(c)
    end
  end

  def pixel_hue(pixel, overlay = 0)
    ((ChunkyPNG::Color.to_hsb(pixel)[0] + overlay) % 360) +
      ChunkyPNG::Color.to_hsb(pixel)[2]
  end

  def pixel_group(pixel, range)
    if @detection_type == 'lightness_range'
      min = range == 2 ? @detection_min_2 : @detection_min
      max = range == 2 ? @detection_max_2 : @detection_max
      lightness = ChunkyPNG::Color.to_hsl(pixel)[2]
      (lightness * 100) > min.to_f && (lightness * 100) < max.to_f
    else
      Helper.hsl_to_color_group(pixel)
    end
  end

  def sort_by_saturation(strip)
    strip.sort_by do |c|
      @inverted * ((ChunkyPNG::Color.to_hsb(c)[1]) +
                   (ChunkyPNG::Color.to_hsb(c)[2]))
    end
  end
end
