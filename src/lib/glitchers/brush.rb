require_relative '../rusty_engine/rusty_engine.rb'
class Brush
  def initialize(options = {})
    @direction = options[:direction] || :horizontal_inverted
    @probability = (options[:probability] || 18).to_s
    @min_pixels = (options[:min_pixels] || 1).to_s
    @max_pixels = (options[:max_pixels] || 10).to_s
  end

  def call(input_image, output_image)
    ruby_to_rust_directions = { horizontal: '1', vertical: '2',
                                horizontal_inverted: '3',
                                vertical_inverted: '4' }
    RustyEngine.brush(input_image, output_image, @probability, @min_pixels,
                      @max_pixels, ruby_to_rust_directions[@direction])
  end
end
