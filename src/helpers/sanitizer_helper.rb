module SanitizerHelper
  def self.unsanitize_value(value)
    value.capitalize.gsub('_', ' ')
  end

  def self.sanitize_value(value)
    value.downcase.gsub(' ', '_')
  end

  def self.bool_to_yes_no(bool)
    bool ? 'Yes' : 'No'
  end

  def self.underscore(camel_cased_word)
    camel_cased_word.to_s.gsub(/::/, '/').
      gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
      gsub(/([a-z\d])([A-Z])/,'\1_\2').
      tr("-", "_").
      downcase
  end
end
