module IoScreensHelper
  def self.define_output_filename(input_file)
    return 'glitcher' if input_file.nil? || input_file.empty?

    File.basename(input_file, '.png').sub('.PNG', '')
  end
end
