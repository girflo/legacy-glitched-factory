module ColorsHelper
  SECONDARY = rgb(40, 52, 74)
  GREEN = rgb(52, 74, 40)
  RED = rgb(74, 40, 52)
  GREY = rgb(47, 47, 47)
  MEDIUM_GREY = rgb(38, 38, 38)
  TEXT_LIGHT = rgb(243, 243, 243)
  BACKGROUND = rgb(18, 18, 18)
  PRIMARY = rgb(62, 40, 74)
end
