class ApplicationValidator
  require 'json'

  def initialize(_ = {})
    @error_numbers = 0
    @error_messages = []
  end

  protected

  def add_error(text)
    @error_numbers += 1
    @error_messages << CONTENT['validator'][text]
  end

  def valid_json?(input_file)
    file = File.open(input_file)
    json = file.read
    JSON.parse(json)
    return true
  rescue JSON::ParserError
    return false
  end

  def validate_io
    validate_input
    validate_output_folder
    validate_output_file
  end

  def validate_input
    add_error('no_input') if (@input_file.nil? || @input_file.empty?) && (@input_folder.nil? || @input_folder.empty?)

    validate_input_file_type unless @input_file.nil? || @input_file.empty?
  end

  def validate_multiple_input_images
    pngs = Dir.open("#{@input_folder}")
            .entries
            .select { |child| ['.png', '.PNG'].include? File.extname(child) }
            .count
    return if pngs > 1

    add_error('input_folder_should_contain_multiple_images')
  end

  def validate_multiple_input_images_jpg
    images = Dir.open("#{@input_folder}")
               .entries
               .select { |child| ['.jpg', '.JPG', '.jpeg', 'JPEG'].include? File.extname(child) }
               .count
    return if images > 1

    add_error('input_folder_should_contain_multiple_jpg_images')
  end

  def validate_input_file_type
    return if @input_file.end_with?(".png") || @input_file.end_with?(".PNG")

    add_error('input_file_not_png')
  end

  def validate_input_file_type_jpg
    return if ['.jpg', '.jpeg', '.JPG', '.JPEG'].include? File.extname(@input_file)

    add_error('input_file_not_jpg')
  end

  def validate_output_folder
    add_error('no_output_folder') if @output_folder.nil? || @output_folder.empty?
  end

  def validate_output_file
    add_error('empty_output_filename') if @output_filename.nil?
  end
end
