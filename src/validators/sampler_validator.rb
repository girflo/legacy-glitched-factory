require_relative 'application_validator.rb'

class SamplerValidator < ApplicationValidator
  def initialize(options = {})
    @input_file = options[:input_file]
    @output_folder = options[:output_folder]
    super
  end

  def call
    validate_input
    validate_output_folder

    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end
end
