require_relative 'application_validator.rb'

class ProfileExporterValidator < ApplicationValidator
  def initialize(options = {})
    @output_folder = options[:output_folder]
    super
  end

  def call
    add_error('no_output_folder') if @output_folder.nil?
    add_error('inexisting_output_folder') unless File.directory?(@output_folder)

    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end
end
