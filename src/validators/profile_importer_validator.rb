require_relative 'application_validator.rb'

class ProfileImporterValidator < ApplicationValidator
  def initialize(options = {})
    @input_file = options[:input_file]
    super
  end

  def call
    validate_input_file
    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end

  private

  def validate_input_file
    return add_error('no_input') if @input_file.nil? || @input_file.empty?

    add_error('file_not_json') unless valid_json?(@input_file)
  end
end
