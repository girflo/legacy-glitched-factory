require_relative 'application_validator.rb'

class ConverterValidator < ApplicationValidator
  def initialize(options = {})
    @input_file = options[:input_file]
    @input_folder = options[:input_folder]
    @output_folder = options[:output_folder]
    super
  end

  def call
    add_error('no_input') if (@input_file.nil? || @input_file.empty?) && (@input_folder.nil? || @input_folder.empty?)
    validate_multiple_input_images_jpg unless @input_folder.nil? || @input_folder.empty?
    validate_input_file_type_jpg unless @input_file.nil? || @input_file.empty?
    validate_output_folder

    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end
end
