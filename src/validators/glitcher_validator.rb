require_relative 'application_validator.rb'

class GlitcherValidator < ApplicationValidator
  def initialize(options = {})
    @validator = options[:validator]
    @input_file = options[:input_file]
    @input_folder = options[:input_folder]
    @output_folder = options[:output_folder]
    @output_filename = options[:output_filename]
    @algorithm = options[:algorithm]
    @range = options[:range]
    @slim_proba = options[:slim_proba]
    @slim_proba_area = options[:slim_proba_area]
    @slim_colors_with_proba = options[:slim_colors_with_proba]
    @brush_direction = options[:brush_direction]
    @brush_probability = options[:brush_probability]
    @brush_min_pixels = options[:brush_min_pixels]
    @brush_max_pixels = options[:brush_max_pixels]
    @byte_numbers = options[:byte_numbers]
    @range_min = options[:range_min]
    @range_max = options[:range_max]
    @multiple_ranges = options[:multiple_ranges]
    @smart_sorting = options[:smart_sorting]
    @range_2_min = options[:range_2_min]
    @range_2_max = options[:range_2_max]
    @iterations = options[:iterations]
    super
  end

  def call
    validate_io
    if @validator == 'Exchange'
      validate_range
    elsif @validator == 'Defect'
      validate_iterations
    elsif @validator == 'Slim'
      validate_slim_proba
    elsif @validator == 'ChangeByte'
      validate_byte
    elsif @validator == 'Brush'
      validate_brush
    elsif @validator == 'PixelSorter'
      validate_pixel_sorter
    elsif @validator == 'Sorter'
      validate_sorter
    end
    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end

  private

  def validate_brush
    validate_proba(@brush_probability)
    validate_brush_pixels
  end

  def validate_pixel_sorter
    validate_sorting_range if @smart_sorting
    validate_sorting_range_2 if @smart_sorting
  end

  def validate_byte
    add_error('byte_should_be_number') unless numeric?(@byte_numbers)
    add_error('byte_should_be_more_than_zero') if @byte_numbers.to_i < 0
  end

  def validate_slim_proba
    if @slim_proba_area == 'global'
      validate_proba(@slim_proba)
    else
      @slim_colors_with_proba.each do |proba|
        validate_proba(proba)
      end
    end
  end

  def validate_brush_pixels
    add_error('min_pixel_should_be_number') unless numeric?(@brush_min_pixels)
    add_error('max_pixel_should_be_number') unless numeric?(@brush_max_pixels)
    add_error('min_pixel_should_be_more_than_zero') if @brush_min_pixels.to_i < 0
    add_error('max_pixel_should_be_more_than_one') if @brush_max_pixels.to_i < 1
    add_error('max_pixel_should_be_more_than_min') if @brush_max_pixels.to_i < @brush_min_pixels.to_i
  end

  def validate_proba(proba)
    return add_error('probability_should_be_number') unless numeric?(proba)

    add_error('probability_should_be_more_than_zero') if proba.to_i <= 0
    add_error('probability_should_be_less_than_hundred') if proba.to_i > 100
  end

  def validate_range
    return if @range.nil?

    add_error('range_should_be_zero_or_more') if !/\A\d+\z/.match(@range)
  end

  def validate_iterations
    return add_error('iterations_should_be_number') unless numeric?(@iterations)

    add_error('iterations_should_be_more_than_zero') if @iterations.to_i <= 0
  end

  def validate_sorting_range
    return if (@range_min.nil? || @range_max.nil?) && @smart_sorting

    add_error('min_range_should_be_number') unless numeric?(@range_min)
    add_error('max_range_should_be_number') unless numeric?(@range_max)
    add_error('min_range_should_be_zero_or_more') if @range_min.to_i < 0
    add_error('max_range_should_be_one_or_more') if @range_max.to_i < 1
    add_error('max_range_should_be_hundred_or_less') if @range_max.to_i > 100
    add_error('max_range_should_be_more_than_min') if @range_max.to_i < @range_min.to_i
  end

  def validate_sorting_range_2
    return if @multiple_ranges != true || @range_2_min.nil? || @range_2_max.nil?

    add_error('min_range_should_be_number')  unless numeric?(@range_2_min)
    add_error('max_range_should_be_number') unless numeric?(@range_2_max)
    add_error('min_range_should_be_zero_or_more') if @range_2_min.to_i < 0
    add_error('max_range_should_be_one_or_more') if @range_2_max.to_i < 1
    add_error('max_range_should_be_hundred_or_less') if @range_2_max.to_i > 100
    add_error('max_range_should_be_more_than_min') if @range_2_max.to_i < @range_2_min.to_i
  end

  def numeric?(string)
    !!Float(string) rescue false
  end
end
