require_relative 'application_validator.rb'

class SorterValidator < ApplicationValidator
  def initialize(options = {})
    @input_folder = options[:input_folder]
    @output_folder = options[:output_folder]
    @output_filename = options[:output_filename]
    super
  end

  def call
    validate_multiple_input_images
    validate_output_folder
    validate_output_file

    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end
end
