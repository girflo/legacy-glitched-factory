require_relative 'application_validator.rb'

class ProfileValidator < ApplicationValidator
  def initialize(options = {})
    @profile_name = options[:profile_name]
    @profile_key = options[:profile_key]
    super
  end

  def call
    add_error('profile_name_empty') if @profile_name.nil? || @profile_name.empty?
    add_error('profile_name_already_used') if ProfileAccessor.check_name_existence(@profile_name.to_s, @profile_key)
    { error_numbers: @error_numbers,
      error_message: @error_messages }
  end
end
