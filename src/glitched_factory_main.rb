# coding: utf-8
Shoes.setup do
  gem 'pnglitch'
  gem 'chunky_png'
  gem 'concurrent-ruby'
end
require 'yaml'
require_relative 'views/screens/about_screen.rb'
require_relative 'views/screens/convert_screen.rb'
require_relative 'views/screens/help_glitcher_screen.rb'
require_relative 'views/screens/help_sampler_screen.rb'
require_relative 'views/screens/help_sorter_screen.rb'
require_relative 'views/screens/help_queue_screen.rb'
require_relative 'views/screens/help_profiles_screen.rb'
require_relative 'views/screens/glitcher_screen.rb'
require_relative 'views/screens/queue_screen.rb'
require_relative 'views/screens/sample_screen.rb'
require_relative 'views/screens/settings_screen.rb'
require_relative 'views/screens/sorting_screen.rb'
require_relative 'views/screens/application_screen.rb'

APPLICATION = YAML.load_file('application.yml')
CONTENT = YAML.load_file('static/content.yml')
@@settings = YAML.load_file('settings.yml')
@@jobs_queue = JobsQueue::Manager.new

class Ui < Shoes
  url '/',              :glitcher_page
  url '/about',         :about_page
  url '/convert',       :convert_page
  url '/help_glitcher', :help_glitcher_page
  url '/help_sampler',  :help_sampler_page
  url '/help_sorter',   :help_sorter_page
  url '/help_queue',    :help_queue_page
  url '/help_profiles', :help_profiles_page
  url '/queue',         :queue_page
  url '/sample',        :sample_page
  url '/settings',      :settings_page
  url '/sorting',       :sorting_page

  def abstract_page(screen)
    background ColorsHelper::BACKGROUND
    application_screen(screen)
  end

  def about_page
    abstract_page(about_screen)
  end

  def convert_page
    abstract_page(convert_screen)
  end

  def help_glitcher_page
    abstract_page(help_glitcher_screen)
  end

  def help_sampler_page
    abstract_page(help_sampler_screen)
  end

  def help_sorter_page
    abstract_page(help_sorter_screen)
  end

  def help_queue_page
    abstract_page(help_queue_screen)
  end

  def help_profiles_page
    abstract_page(help_profiles_screen)
  end

  def glitcher_page
    abstract_page(glitcher_screen)
  end

  def queue_page
    abstract_page(queue_screen)
  end

  def sample_page
    abstract_page(sample_screen)
  end

  def settings_page
    abstract_page(settings_screen)
  end

  def sorting_page
    abstract_page(sorting_screen)
  end
end

Shoes.app(app_name: 'GlitchedFactory', title: 'GlitchedFactory', width: 900,
          height: 800, menus: true)
