# coding: utf-8
class TemplateMessages < Shoes::Widget
  def hide_all
    @errors_stack.hide
    @success_stack.hide
    @container_stack.hide
  end

  def show_success(kind)
    message = success_message(kind)
    @success_message.text = message
    @success_message.stroke = ColorsHelper::TEXT_LIGHT
    @container_stack.show
    @success_stack.show
    timer(4) do
      @success_stack.hide
      @container_stack.hide
    end
  end

  def show_errors(errors)
    @errors.text = errors.each_with_index.map do |m, i|
      if i == errors.size - 1
        "◦ #{m}"
      else
        "◦ #{m}
"
      end
    end
    @errors.stroke = ColorsHelper::TEXT_LIGHT
    @container_stack.show
    @errors_stack.show
  end

  def initialize(error_title)
    @container_stack = stack do
      @errors_stack = template_errors(error_title)
      @success_stack = template_success
    end
    @container_stack.hide
    @errors_stack.hide
    @success_stack.hide
  end

  private

  def template_errors(title)
    stack do
      stack margin_bottom: 20 do
        background ColorsHelper::RED
        stack margin_left: 20, margin_top: 10 do
          template_paragraph(title)
          stack margin_left: 20 do
            @errors = para ''
          end
        end
      end
    end
  end

  def template_success
    stack do
      stack margin_bottom: 20 do
        background ColorsHelper::GREEN
        stack margin_left: 20, margin_top: 10 do
          @success_message = para ''
        end
      end
    end
  end

  def success_message(kind)
    if kind == 'multiple_images'
      CONTENT['success_multiple']
    elsif kind == 'single_image'
      CONTENT['success_single']
    elsif kind == 'settings'
      CONTENT['settings']['success']
    elsif kind == 'profile'
      CONTENT['profile']['success']
    elsif kind == 'profiles_export'
      CONTENT['profiles_export']['success']
    elsif kind == 'profiles_import'
      CONTENT['profiles_import']['success']
    end
  end
end
