class TemplateSubtitleCentered < Shoes::Widget
  def initialize(text)
    @text = subtitle text
    @text.align = 'center'
    @text.stroke = ColorsHelper::TEXT_LIGHT
  end
end
