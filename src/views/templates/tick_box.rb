class TemplateTickBox < Shoes::Widget
  def initialize(ticked, text)
    self.width = 120
    @tick_box = check checked: ticked; colorize_text(text)
  end

  def colorize_text(text)
    paragraph = para text
    paragraph.stroke = ColorsHelper::TEXT_LIGHT
    paragraph
  end

  def checked?
    @tick_box.checked?
  end
end
