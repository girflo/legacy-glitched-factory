class TemplateSettingsTipsVisibility < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      template_attribute_title('Tips : ')
      @visibility = true
      list_box items: ['Visible', 'Hidden'], choose: current_tips_visibility do |visibility|
        @visibility = value_to_bool(visibility.text.to_s)
      end
    end
  end

  def setting
    @visibility
  end

  private

  def current_tips_visibility
    @@settings['tips_visible'] ? 'Visible' : 'Hidden'
  end

  def value_to_bool(value)
    value == 'Visible'
  end
end
