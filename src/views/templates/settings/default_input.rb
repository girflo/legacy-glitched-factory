class TemplateSettingsDefaultInput < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      @default_input = @@settings['default_input']
      template_attribute_title('Default glitcher input :')
      button 'Default image' do
        @default_input = ask_open_file title: 'Choose the image you want to use as default'
        @default_input_type = 'file'
        @display_default_input.clear { display_current_setting(@default_input) }
      end
      template_paragraph_small('  OR  ')
      button 'Default directory' do
        @default_input = ask_open_folder title: 'Choose the directory you want to glitch'
        @default_input_type = 'directory'
        @display_default_input.clear { display_current_setting(@default_input) }
      end
      template_attribute_title(clear_link)
      @display_default_input = display_current_setting(@default_input)
    end
  end

  def clear_link
    link('Clear defaut glitcher input', click: proc { clear_default_input } )
  end

  def clear_default_input
    @display_default_input.clear { display_current_setting('') }
    @default_input = ''
  end

  def input
    @default_input
  end

  def type
    @default_input_type
  end

  def display_current_setting(value)
    if value.nil? || value.empty?
      display_value = 'Unset'
    else
      display_value = value
    end

    flow do
      template_attribute_title('Currently :')
      template_paragraph_large("#{display_value}")
    end
  end
end
