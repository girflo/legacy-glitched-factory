class TemplateSettingsQueueAutoreload < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      template_attribute_title('Autoreload queue : ')
      @autoreload = true
      list_box items: ['Yes', 'No'], choose: current_queue_autoreload do |autoreload|
        @autoreload = value_to_bool(autoreload.text.to_s)
      end
    end
  end

  def setting
    @autoreload
  end

  private

  def current_queue_autoreload
    @@settings['queue_autoreload'] ? 'Yes' : 'No'
  end

  def value_to_bool(value)
    value == 'Yes'
  end
end
