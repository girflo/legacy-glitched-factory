class TemplateSettingsAlgorithmInName < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      template_attribute_title('Add library name in glitcher\'s output filename : ')
      @algorithm_in_name = true
      list_box items: ['Yes', 'No'], choose: current_algorithm_in_name do |algorithm_in_name|
        @algorithm_in_name = value_to_bool(algorithm_in_name.text.to_s)
      end
    end
  end

  def setting
    @algorithm_in_name
  end

  private

  def current_algorithm_in_name
    @@settings['algorithm_in_name'] ? 'Yes' : 'No'
  end

  def value_to_bool(value)
    value == 'Yes'
  end
end
