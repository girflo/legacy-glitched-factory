class TemplateSettingsDefaultOutput < Shoes::Widget
  attr_reader :output

  def initialize
    flow margin_left: 20 do
      @output = @@settings['default_output']
      template_attribute_title('Default output :')
      button 'Default directory' do
        @output = ask_open_folder title: 'Choose the directory you want to glitch'
        @display_default_output.clear { display_current_setting(@output) }
      end
      template_attribute_title(clear_link)
      @display_default_output = display_current_setting(@output)
    end
  end

  def clear_link
    link('Clear defaut output', click: proc { clear_default_output })
  end

  def clear_default_output
    @display_default_output.clear { display_current_setting('') }
    @output = ''
  end

  def display_current_setting(value)
    if value.nil? || value.empty?
      display_value = 'Unset'
    else
      display_value = value
    end

    flow do
      template_attribute_title('Currently :')
      template_paragraph_large("#{display_value}")
    end
  end
end
