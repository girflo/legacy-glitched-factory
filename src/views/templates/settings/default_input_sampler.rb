class TemplateSettingsDefaultInputSampler < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      @default_input_sampler = @@settings['default_input_sampler']
      template_attribute_title('Default sampler input image :')
      button 'Default image' do
        @default_input_sampler = ask_open_file title: 'Choose the image you want to use as default'
        @display_default_input.clear { display_current_setting(@default_input_sampler) }
      end
      template_attribute_title(clear_link)
      @display_default_input = display_current_setting(@default_input_sampler)
    end
  end

  def clear_link
    link('Clear defaut sampler input', click: proc { clear_default_input_sampler })
  end

  def clear_default_input_sampler
    @display_default_input.clear { display_current_setting('') }
    @default_input_sampler = ''
  end

  def input
    @default_input_sampler
  end

  def display_current_setting(value)
    if value.nil? || value.empty?
      display_value = 'Unset'
    else
      display_value = value
    end

    flow do
      template_attribute_title('Currently :')
      template_paragraph_large("#{display_value}")
    end
  end
end
