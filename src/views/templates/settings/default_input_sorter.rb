class TemplateSettingsDefaultInputSorter < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      @default_input_sorter = @@settings['default_input_sorter']
      template_attribute_title('Default sorter input directory :')
      button 'Default directory' do
        @default_input_sorter = ask_open_folder title: 'Choose the directory you want to use as default'
        @display_default_input.clear { display_current_setting(@default_input_sorter) }
      end
      template_attribute_title(clear_link)
      @display_default_input = display_current_setting(@default_input_sorter)
    end
  end

  def clear_link
    link('Clear defaut sorter input', click: proc { clear_default_input_sorter } )
  end

  def clear_default_input_sorter
    @display_default_input.clear { display_current_setting('') }
    @default_input_sorter = ''
  end

  def input
    @default_input_sorter
  end

  def display_current_setting(value)
    if value.nil? || value.empty?
      display_value = 'Unset'
    else
      display_value = value
    end

    flow do
      template_attribute_title('Currently :')
      template_paragraph_large("#{display_value}")
    end
  end
end
