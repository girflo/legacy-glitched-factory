class TemplateSettingsCompactInterface < Shoes::Widget
  def initialize
    flow margin_left: 20 do
      template_attribute_title('Compact interface : ')
      @compact_interface = true
      list_box items: ['Yes', 'No'], choose: current_compact_interface do |compact_interface|
        @compact_interface = value_to_bool(compact_interface.text.to_s)
      end
    end
  end

  def setting
    @compact_interface
  end

  private

  def current_compact_interface
    @@settings['compact_interface'] ? 'Yes' : 'No'
  end

  def value_to_bool(value)
    value == 'Yes'
  end
end
