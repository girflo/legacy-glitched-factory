require_relative '../../helpers/colors_helper.rb'

class TemplateSectionSubtitle < Shoes::Widget
  def initialize(title)
    @section_title = tagline title
    @section_title.size = element_size
    @section_title.stroke = ColorsHelper::TEXT_LIGHT
  end

  private

  def element_size
    @@settings['compact_interface'] ? 12 : 18
  end
end
