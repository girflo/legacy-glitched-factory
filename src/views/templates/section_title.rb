require_relative '../../helpers/colors_helper.rb'

class TemplateSectionTitle < Shoes::Widget
  def initialize(title, small_margin = false)
    self.margin_top = margin_top_size(small_margin)
    @section_title = subtitle title
    @section_title.size = element_size
    @section_title.stroke = ColorsHelper::TEXT_LIGHT
  end

  private

  def element_size
    @@settings['compact_interface'] ? 18 : 26
  end

  def margin_top_size(small_margin)
    (small_margin || @@settings['compact_interface']) ? 0 : 20
  end
end
