class TemplateConvertIo < Shoes::Widget
  def params
    { input_file: @input_file,
      input_folder: @input_folder,
      output_folder: @output_folder,
      output_filename: '' }
  end

  def initialize
    if @@settings['default_input_type'] == 'directory'
      @input_folder = @@settings['default_input']
      @input_file = ''
    elsif @@settings['default_input_type'] == 'file'
      @input_folder = ''
      @input_file = @@settings['default_input']
    end
    @output_folder = @@settings['default_output']
    template_input
    template_output
  end

  def template_input
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title(CONTENT['io']['input'], true)
        flow margin_left: 20 do
          button CONTENT['io']['image_convert'] do
            @input_file = ask_open_file title: CONTENT['io']['image_convert_long']
            update_preview(@input_file)
          end
          template_paragraph_small(CONTENT['io']['or'])
          button CONTENT['io']['directory_convert'] do
            @input_folder = ask_open_folder title: CONTENT['io']['directory_convert_long']
            update_preview_directory(@input_folder)
          end
          @choosed_input = default_input_display
        end
      end
    end
  end

  def template_output
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title(CONTENT['io']['output'])
        flow margin_left: 20 do
          flow do
            button CONTENT['io']['output_directory'] do
              @output_folder = ask_open_folder
              @choosed_output.clear do
                flow do
                  template_attribute_title(CONTENT['io']['selected_directory'])
                  template_paragraph_large("#{@output_folder}")
                end
              end
            end
          end
          @choosed_output = stack { template_paragraph_large("#{@output_folder}") }
        end
      end
    end
  end

  private

  def update_preview(path)
    return if path.nil?

    @choosed_input.clear { template_input_file_preview(path) }
  end

  def update_preview_directory(path)
    @choosed_input.clear { template_input_folder_preview(path, :jpg) }
  end

  def default_input_display
    return (stack {}) if (@input_folder.nil? || @input_folder.empty?) && (@input_file.nil? || @input_file.empty?)

    if @input_file.nil? || @input_file.empty?
      stack { template_input_folder_preview(@input_folder) }
    else
      stack { template_input_file_preview(@input_file) }
    end
  end
end
