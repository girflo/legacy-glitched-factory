class TemplateMenus < Shoes::Widget
  def initialize
    return unless menubar.menus.size == 1

    helpmenu = menu "Help"
    @help_glitcher_item = menuitem "Help Glitcher" do
      visit '/help_glitcher'
    end
    @help_sampler_item = menuitem "Help Sampler" do
      visit '/help_sampler'
    end
    @help_sorter_item = menuitem "Help Sorter" do
      visit '/help_sorter'
    end
    @help_queue_item = menuitem "Help Queue" do
      visit '/help_queue'
    end
    @help_profiles_item = menuitem "Help Profiles" do
      visit '/help_profiles'
    end
    @aboutitem = menuitem "About" do
      visit '/about'
    end
    helpmenu << @help_glitcher_item
    helpmenu << @help_sampler_item
    helpmenu << @help_sorter_item
    helpmenu << @help_queue_item
    helpmenu << @help_profiles_item
    helpmenu << @aboutitem
    glitchermenu = menu "Go"
    @glitcheritem = menuitem "Glitcher" do
      visit '/'
    end
    @sampleritem = menuitem "Sampler" do
      visit '/sample'
    end
    @sortingitem = menuitem "Pixel sorting" do
      visit '/sorting'
    end
    @queueitem = menuitem "Queue" do
      visit '/queue'
    end
    @settingsitem = menuitem "Settings" do
      visit '/settings'
    end
    glitchermenu << @glitcheritem
    glitchermenu << @sampleritem
    glitchermenu << @sortingitem
    glitchermenu << @queueitem
    glitchermenu << @settingsitem
    menubar << glitchermenu
    menubar << helpmenu
  end
end
