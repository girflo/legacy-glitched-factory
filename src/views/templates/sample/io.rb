class TemplateSampleIo < Shoes::Widget
  attr_reader :input_file, :output_folder

  def output_filename
    @output_filename.text
  end

  def initialize(suffix_filename)
    @input_file = @@settings['default_input_sampler']
    @output_folder = @@settings['default_output']
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title(CONTENT['io']['input'], true)
        flow margin_left: 20 do
          button CONTENT['io']['image_sample'] do
            input_file = ask_open_file title: CONTENT['io']['directory_convert_long']
            unless input_file.nil? || input_file.empty?
              @input_file = input_file
              @output_filename.text = IoScreensHelper.define_output_filename(@input_file)
              update_preview(@input_file)
            end
          end
        end
        flow margin_left: 20 do
          @choosed_input = default_input_display
        end
      end
    end
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title(CONTENT['io']['output'])
        stack margin_left: 20 do
          flow do
            button CONTENT['io']['output_directory'] do
              @output_folder = ask_open_folder
              @choosed_output.clear { display_output(@output_folder) }
            end
          end
          @choosed_output = display_output(@output_folder)
          flow do
            template_attribute_title(CONTENT['io']['output_name_schema'])
            @output_filename = edit_line
            @output_filename.text = IoScreensHelper.define_output_filename(@input_file)
            template_paragraph_medium(suffix_filename)
          end
        end
      end
    end
  end

  private

  def default_input_display
    return (stack {}) if @input_file.nil? || @input_file.empty?

    stack { template_input_file_preview(@input_file) }
  end

  def display_output(value)
    return (stack {}) if value.nil? || value.empty?

    flow do
      template_attribute_title(CONTENT['io']['selected_directory'])
      template_paragraph_large("#{value}")
    end
  end

  def update_preview(path)
    return if path.nil?

    @choosed_input.clear { template_input_file_preview(path) }
  end
end
