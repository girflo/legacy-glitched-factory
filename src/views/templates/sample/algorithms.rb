class TemplateSampleAlgorithms < Shoes::Widget
  def algorithms
    @selected_algorithms.map { |c, name| name.downcase.gsub(' ', '_') if c.checked? }.compact
  end

  def initialize
    template_section_subtitle('Algorithms')
    stack margin_left: 20 do
      all_algorithms = ['Exchange', 'Defect', 'Transpose', 'Wrong filter', 'Slim', 'Brush', 'Change byte']
      flow do
        @selected_algorithms = all_algorithms.map do |name|
          @c = template_tick_box(true, name)
          [@c, name]
        end
      end
    end
  end
end
