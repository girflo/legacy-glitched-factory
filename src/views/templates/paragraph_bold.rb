class TemplateParagraphBold < Shoes::Widget
  def initialize(text)
    @text = para text, displace_left: 10
    @text.stroke = ColorsHelper::TEXT_LIGHT
    @text.weight = 'bold'
  end
end
