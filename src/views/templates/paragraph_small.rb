class TemplateParagraphSmall < Shoes::Widget
  def initialize(text)
    self.width = 60
    @text = para text
    @text.stroke = ColorsHelper::TEXT_LIGHT
  end
end
