class TemplateSortingIo < Shoes::Widget
  attr_reader :input_folder, :output_folder

  def output_filename
    @output_filename.text
  end

  def initialize(suffix_filename, filename = '')
    @input_folder = @@settings['default_input_sorter']
    @output_folder = @@settings['default_output']
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title('Input', true)
        flow margin_left: 20 do
          button 'Directory to sort' do
            @input_folder = ask_open_folder title: 'Choose the directory you want to sort'
            update_preview(@input_folder) unless @input_folder.nil? || @input_folder.empty?
          end
          @choosed_input = default_input_display
        end
      end
    end
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title('Output')
        stack margin_left: 20 do
          flow do
            button 'Output directory' do
              @output_folder = ask_open_folder
              @choosed_output.clear { display_output(@output_folder) }
            end
          end
          @choosed_output = display_output(@output_folder)
          flow do
            template_attribute_title('Output name schema : ')
            @output_filename = edit_line
            @output_filename.text = filename
            template_paragraph_medium(suffix_filename)
          end
        end
      end
    end
  end

  private

  def default_input_display
    return (stack {}) if @input_folder.nil? || @input_folder.empty?

    stack { template_input_folder_preview(@input_folder) }
  end

  def display_output(value)
    return (stack {}) if value.nil? || value.empty?

    flow do
      template_attribute_title("Selected directory : ")
      template_paragraph_large("#{value}")
    end
  end

  def update_preview(path)
    @choosed_input.clear { template_input_folder_preview(path) }
  end
end
