# coding: utf-8
class TemplateSortingRangeSorting < Shoes::Widget
  def validator_options
    smart_sorting ? validator_options_smart : validator_options_brut
  end

  def glitcher_options
    { direction: direction,
      inverted: sorting_inverted,
      sorting_by: sorting_by,
      detection_type: detection_type,
      detection_min: range_min,
      detection_max: range_max,
      multiple_ranges: multiple_ranges,
      detection_min_2: range_min_2,
      detection_max_2: range_max_2,
      smart_sorting: smart_sorting }
  end

  def initialize(presets = {})
    @direction_preset = presets['direction'] || 'vertical'
    @inverted_preset = presets['inverted'] || false
    @sorting_by_preset = presets['sorting_by'] || 'hue'
    @detection_type_preset = presets['detection_type'] || 'lightness_range'
    @multiple_ranges_preset = true
    @min_1_preset = presets['detection_min'] || '0'
    @max_1_preset = presets['detection_max'] || '50'
    @min_2_preset = presets['detection_min_2'] || '60'
    @max_2_preset = presets['detection_max_2'] || '100'
    @smart_sorting_preset = (presets['smart_sorting'] || presets['smart_sorting'].nil?)
    flow do
      flow do
        template_attribute_title('Inverted sorting : ')
        @sorting_inverted = @inverted_preset
        list_box items: ['No', 'Yes'], choose: SanitizerHelper.bool_to_yes_no(@inverted_preset) do |inverted|
          if inverted.text == 'Yes'
            @sorting_inverted = true
          else
            @sorting_inverted = false
          end
        end
      end
      flow do
        template_attribute_title('Direction : ')
        @direction = list_box items: ['Vertical', 'Horizontal'], choose: SanitizerHelper.unsanitize_value(@direction_preset)
      end
      flow do
        template_attribute_title('Sort by : ')
        @sorting_by = list_box items: ['Hue', 'Saturation'], choose: SanitizerHelper.unsanitize_value(@sorting_by_preset)
      end
      flow do
        template_attribute_title('Detection : ')
        @smart_sorting = @smart_sorting_preset
        smart_text_preset = @smart_sorting ? 'Smart' : 'Brut'
        list_box items: ['Smart', 'Brut'], choose: smart_text_preset do |type|
          if type.text == 'Brut'
            @detection_section.hide
            @smart_sorting = false
          else
            @detection_section.show
            @smart_sorting = true
          end
        end
      end
      @detection_section = template_detection
      @detection_section.hide unless @smart_sorting
    end
  end

  private

  def template_detection
    flow do 
      flow do
        template_attribute_title('Detection type :')
        @detection_type = SanitizerHelper.unsanitize_value(@detection_type_preset)
        list_box items: ['Lightness range', 'Colors'], choose: @detection_type do |type|
          @detection_type = type.text
          if type.text == 'Colors'
            @detection_range_group.hide
          else
            @detection_range_group.show
          end
        end
      end
      @detection_range_group = detection_range_group
      @detection_range_group.hide if @detection_type == 'Colors'
    end
  end

  def detection_range_group
    flow do
      @multiple_ranges = true
      @detection_range_section = detection_range
      @detection_range_2_section = detection_range_2
    end
  end

  def detection_range
    flow do      
      template_attribute_title('Starting range :')
      flow do
        template_attribute_title('Min. :')
        @range_min = edit_line width: 60
        @range_min.text = @min_1_preset
        @range_min.margin_right = 10
        template_paragraph_small('Max. :')
        @range_max = edit_line width: 50
        @range_max.text = @max_1_preset
        template_paragraph_small('/ 100')
      end
    end
  end

  def detection_range_2
    flow do
      template_attribute_title('Ending range :')
      flow do
        template_attribute_title('Min. :')
        @range_2_min = edit_line width: 60
        @range_2_min.text = @min_2_preset
        @range_2_min.margin_right = 10
        template_paragraph_small('Max. :')
        @range_2_max = edit_line width: 50
        @range_2_max.text = @max_2_preset
        template_paragraph_small('/ 100')
      end
    end
  end

  def direction
    @direction.text.downcase
  end

  def detection_type
    @detection_type.downcase.gsub(' ', '_')
  end

  def sorting_by
    @sorting_by.text.downcase
  end

  def smart_sorting
    @smart_sorting
  end

  def range_min
    @range_min.text
  end

  def range_max
    @range_max.text
  end

  def range_min_2
    @range_2_min.text
  end

  def range_max_2
    @range_2_max.text
  end

  def multiple_ranges
    @multiple_ranges
  end

  def sorting_inverted
    @sorting_inverted
  end

  def validator_options_brut
    { smart_sorting: smart_sorting }
  end

  def validator_options_smart
    { smart_sorting: smart_sorting,
      range_min: range_min,
      range_max: range_max,
      multiple_ranges: multiple_ranges,
      range_2_min: range_min_2,
      range_2_max: range_max_2 }
  end
end
