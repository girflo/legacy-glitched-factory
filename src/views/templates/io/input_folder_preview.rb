class TemplateInputFolderPreview < Shoes::Widget
  def initialize(directory_path, img_format = :png)
    @directory_path = directory_path
    img_format == :png ? input_folder_string_png : input_folder_string_jpg
  end

  def input_folder_string_png
    png_files = Dir
                  .open("#{@directory_path}")
                  .entries
                  .select { |child| ['.png', '.PNG'].include? File.extname(child) }
                  .count
    template_attribute_title(CONTENT['io']['selected_directory'])
    if png_files > 1
      template_paragraph_large("#{@directory_path} | Containing #{png_files} png children")
    else
      template_paragraph_large("#{@directory_path} | Containing #{png_files} png child")
    end
  end

  def input_folder_string_jpg
    jpg_files = Dir
                  .open("#{@directory_path}")
                  .entries
                  .select { |child| ['.jpg', '.JPG', '.jpeg', '.JPEG'].include? File.extname(child) }
                  .count
    template_attribute_title(CONTENT['io']['selected_directory'])
    if jpg_files > 1
      template_paragraph_large("#{@directory_path} | Containing #{jpg_files} jpg children")
    else
      template_paragraph_large("#{@directory_path} | Containing #{jpg_files} jpg child")
    end
  end
end
