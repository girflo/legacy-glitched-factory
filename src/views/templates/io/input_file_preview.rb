class TemplateInputFilePreview < Shoes::Widget
  def initialize(file_path)
    if ['.png', '.PNG', '.jpg', '.jpeg', '.JPG', '.JPEG'].include? File.extname(file_path)
      preview_dimensions = preview_dimensions(file_path)
      @preview_image = image file_path, cache: false
      @preview_image.width = preview_dimensions.first
      @preview_image.height = preview_dimensions.last
      @preview_image.displace_left = 8
    else
      template_attribute_title(CONTENT['io']['selected_file'])
      template_paragraph_large(file_path.to_s)
    end
  end

  def preview_dimensions(path)
    preview_scale = preview_scale(path)
    width = imagesize(path).first.to_f * preview_scale
    height = imagesize(path).last.to_f * preview_scale
    [width.ceil, height.ceil]
  end

  def preview_scale(path)
    width = max_preview_dimensions.first / imagesize(path).first.to_f
    height = max_preview_dimensions.last / imagesize(path).last.to_f
    [width, height].min
  end

  def max_preview_dimensions
    [600, 400]
  end
end
