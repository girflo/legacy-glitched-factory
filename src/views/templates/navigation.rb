require_relative '../../helpers/colors_helper.rb'

class TemplateNavigation < Shoes::Widget
  def initialize(current_tab = nil)
    flow margin_top: 12 do
      stack width: small_element_width_size, height: element_height, margin_right: 10 do
        navigation_button(current_tab, 'glitcher', '/')
      end
      stack width: big_element_width_size, height: element_height, margin_right: 10 do
        navigation_button(current_tab, 'sampler', '/sample')
      end
      stack width: big_element_width_size, height: element_height, margin_right: 10 do
        navigation_button(current_tab, 'sorting', '/sorting')
      end
      stack width: big_element_width_size, height: element_height, margin_right: 10 do
        navigation_button(current_tab, 'convert', '/convert')
      end
      stack width: small_element_width_size, height: element_height do
        navigation_button(current_tab, 'queue', '/queue')
      end
    end
    stack margin_bottom: 20 do
      background ColorsHelper::GREY
    end
  end

  private

  def navigation_button(current_tab, tab_name, link)
    if current_tab == tab_name
      navigation_button_selected(tab_name)
    else
      navigation_button_unselected(tab_name, link)
    end
  end

  def navigation_button_selected(tab_name)
    background ColorsHelper::GREY
    @link_background = svg "static/images/nav_#{tab_name}_selected.svg"
  end

  def navigation_button_unselected(tab_name, link)
    background ColorsHelper::MEDIUM_GREY
    @link_background = svg "static/images/nav_#{tab_name}.svg", { click: proc { visit link } }
  end

  def small_element_width_size
    @@settings['compact_interface'] ? 140 : 160
  end

  def big_element_width_size
    @@settings['compact_interface'] ? 140 : 170
  end

  def element_height
    @@settings['compact_interface'] ? 36 : 46
  end
end
