require_relative '../../helpers/colors_helper.rb'

class TemplateBanner < Shoes::Widget
  def initialize
    flow do
      background ColorsHelper::PRIMARY
      inner_banner = flow do
        logo = image 'static/images/banner.png'
        logo.height = 80
        logo.width = 460
      end
      inner_banner.displace_left = 40
    end
  end
end
