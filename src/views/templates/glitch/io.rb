class TemplateGlitchIo < Shoes::Widget
  attr_reader :input_file, :input_folder, :output_folder

  def output_filename
    @output_filename.text
  end

  def params
    { input_file: @input_file,
      input_folder: @input_folder,
      output_folder: @output_folder,
      output_filename: @output_filename.text }
  end

  def initialize(action)
    @prefix_filename = define_output_prefix
    @suffix_filename = '.png'
    if @@settings['default_input_type'] == 'directory'
      @input_folder = @@settings['default_input']
      @input_file = ''
    elsif @@settings['default_input_type'] == 'file'
      @input_folder = ''
      @input_file = @@settings['default_input']
    end
    @output_folder = @@settings['default_output']
    template_input(action)
    template_output
  end

  def template_input(action)
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title(CONTENT['io']['input'], true)
        flow margin_left: 20 do
          button "Image to #{action}" do
            input_file = ask_open_file title: "Choose the image you want to #{action}"
            unless input_file.nil? || input_file.empty?
              @input_file = input_file
              @output_filename.text = IoScreensHelper.define_output_filename(@input_file)
              update_preview(@input_file)
            end
          end
          template_paragraph_small(CONTENT['io']['or'])
          button "Directory to #{action}" do
            input_folder = ask_open_folder title: "Choose the directory you want to #{action}"
            unless input_folder.nil? || input_folder.empty?
              @input_folder = input_folder
              @output_filename.text = ''
              update_preview_directory(@input_folder)
            end
          end
          @choosed_input = default_input_display
        end
      end
    end
  end

  def template_output
    stack do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        template_section_title(CONTENT['io']['output'])
        flow margin_left: 20 do
          flow do
            button CONTENT['io']['output_directory'] do
              @output_folder = ask_open_folder
              @choosed_output.clear do
                template_attribute_title(CONTENT['io']['selected_directory'])
                template_paragraph_large("#{@output_folder}")
              end
            end
          end
          @choosed_output = flow { template_paragraph_large("#{@output_folder}") }
          flow do
            template_attribute_title(CONTENT['io']['output_name_schema'])
            template_paragraph_medium(@prefix_filename)
            @output_filename = edit_line
            @output_filename.text = IoScreensHelper.define_output_filename(@input_file)
            @suffix_file = template_paragraph_small(@suffix_filename)
            @suffix_directory = template_paragraph_medium('filename.png')
            @suffix_directory.hide
          end
        end
      end
    end
  end

  private

  def define_output_prefix
    @@settings['algorithm_in_name'] ? "algorithm_" : ''
  end

  def update_preview(path)
    return if path.nil?

    @choosed_input.clear { template_input_file_preview(path) }
    @suffix_file.show
    @suffix_directory.hide
  end

  def update_preview_directory(path)
    @choosed_input.clear { template_input_folder_preview(path) }
    @suffix_file.hide
    @suffix_directory.show
  end

  def default_input_display
    return (stack {}) if (@input_folder.nil? || @input_folder.empty?) && (@input_file.nil? || @input_file.empty?)

    if @input_file.nil? || @input_file.empty?
      stack { template_input_folder_preview(@input_folder) }
    else
      stack { template_input_file_preview(@input_file) }
    end
  end
end
