class TemplateGlitchAlgorithmsWrongFilter < Shoes::Widget
  def library
    'WrongFilter'
  end

  def validator_options
    {}
  end

  def glitcher_options
    { filter: filter,
      algorithm: 'wrong_filter' }
  end

  def initialize(presets = {})
    @filter_preset = presets['filter'] || 'average'
    flow do
      list_items = %w(None Sub Up Average Paeth Random)
      flow do
        template_attribute_title('Filter to use : ')
        @wrong_filter = list_box items: list_items, choose: @filter_preset.capitalize
      end
    end
  end

  private

  def filter
    @wrong_filter.text.downcase
  end
end
