class TemplateGlitchAlgorithmsTranspose < Shoes::Widget
  def library
    'Transpose'
  end

  def validator_options
    {  }
  end

  def glitcher_options
    { algorithm: 'transpose',
      filter: filter,
      transpose_force: force }
  end

  def initialize(presets = {})
    @force_preset = presets['force'] || 'half'
    @filter_preset = presets['filter'] || 'average'
    flow do
      flow do
        template_attribute_title('Transpose force : ')
        @force = list_box items: ['Half', 'Full'], choose: @force_preset.capitalize
      end
      template_filter
    end
  end

  private

  def template_filter
    list_items = %w(None Sub Up Average Paeth Random)
    flow do
      template_attribute_title('Filter to use : ')
      @filter = list_box items: list_items, choose: @filter_preset.capitalize
    end
  end

  def force
    @force.text.downcase
  end

  def filter
    @filter.text.downcase
  end
end
