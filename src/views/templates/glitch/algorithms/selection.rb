class TemplateGlitchAlgorithmsSelection < Shoes::Widget
  def initialize(presets = {})
    flow do
      @glitch_options = nil
      @algorithm_preset = presets['algorithm'] || 'exchange'
      @algorithm = SanitizerHelper.sanitize_value(@algorithm_preset)
      template_attribute_title('Algorithm to use : ')
      list_box items: ['Exchange', 'Defect', 'Transpose', 'Wrong filter', 'Slim',
                       'Brush', 'Change byte', 'Pixel sorting'],
               choose: SanitizerHelper.unsanitize_value(@algorithm_preset) do |algorithm|
        @algorithm = SanitizerHelper.sanitize_value(algorithm.text)
        @glitch_options.clear
        @glitch_options = call_algorithm_template(@algorithm, presets)
      end
      @glitch_options = call_algorithm_template(@algorithm, presets)
    end
  end

  def algorithm
    @algorithm
  end

  def glitcher_options
    @glitch_options.glitcher_options
  end

  def library
    @glitch_options.library
  end

  def validator_options
    @glitch_options.validator_options
  end

  private

  def call_algorithm_template(algorithm, presets)
    function_name = "template_glitch_algorithms_#{algorithm}".to_sym
    self.send function_name, presets
  end
end
