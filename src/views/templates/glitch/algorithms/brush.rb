class TemplateGlitchAlgorithmsBrush < Shoes::Widget
  def library
    'Brush'
  end

  def validator_options
    { brush_direction: direction,
      brush_probability: probability,
      brush_min_pixels: min_pixels,
      brush_max_pixels: max_pixels }
  end

  def glitcher_options
    { algorithm: 'brush',
      probability: probability,
      direction: direction.to_sym,
      min_pixels: min_pixels,
      max_pixels: max_pixels }
  end

  def initialize(presets = {})
    @direction_preset = presets['direction'] || 'vertical'
    @probability_preset = presets['probability'] || 18
    @min_pixels_preset = presets['min_pixels'] || 1
    @max_pixels_preset = presets['max_pixels'] || 10
    flow do
      flow do
        template_attribute_title('Brush direction : ')
        @direction = list_box items: ['Vertical', 'Vertical inverted', 'Horizontal', 'Horizontal inverted'], choose: @direction_preset.capitalize
      end
      flow do
        template_attribute_title('Probability of brush : ')
        @probability = edit_line width: 40
        @probability.text = @probability_preset.to_s
        template_paragraph_small('%')
      end
      flow do
        template_attribute_title('Min pixels per brush : ')
        @min_pixels = edit_line width: 40
        @min_pixels.text = @min_pixels_preset.to_s
      end
      flow do
        template_attribute_title('Max pixels per brush : ')
        @max_pixels = edit_line width: 40
        @max_pixels.text = @max_pixels_preset
      end
    end
  end

  private

  def direction
    @direction.text.downcase.gsub(' ', '_')
  end

  def probability
    @probability.text.to_i
  end

  def min_pixels
    @min_pixels.text.to_i
  end

  def max_pixels
    @max_pixels.text.to_i
  end
end
