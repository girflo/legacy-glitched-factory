class TemplateGlitchAlgorithmsSlim < Shoes::Widget
  def library
    'Slim'
  end

  def validator_options
    { slim_proba: probability,
      slim_proba_area: area,
      slim_colors_with_proba: colors_probabilities }
  end

  def glitcher_options
    { algorithm: 'slim',
      probability: probability.to_i,
      probability_area: area,
      direction: direction.to_sym,
      colors: colors,
      colors_with_proba: colors_with_proba }
  end

  def initialize(presets = {})
    @probability_preset = presets['probability'] || 95
    @direction_preset = presets['direction'] || 'up_to_down'
    @area_preset = presets['probability_area'] || 'global'
    @colors_preset = presets['colors'] || all_colors.map(&:downcase)
    @colors_with_proba_preset = presets['colors_with_proba'] || all_colors_with_proba
    flow do
      flow do
        template_attribute_title('Slim direction : ')
        @slim_direction = list_box items: ['Up to down', 'Down to up', 'Left to right', 'Right to left'],
                                   choose: SanitizerHelper.unsanitize_value(@direction_preset)
      end
      @slim_proba_area = para @area_preset.capitalize
      @slim_proba_area.hide
      flow do
        template_attribute_title("Probability's area of effect : ")
        list_box items:['Global', 'Per color'], choose: SanitizerHelper.unsanitize_value(@area_preset) do |proba_area|
          @slim_proba_area.text = SanitizerHelper.sanitize_value(proba_area.text)
          if proba_area.text == 'Global'
            @proba_global_proba.show
            @proba_global_colors.show
            @proba_per_color_options.hide
          else
            @proba_global_proba.hide
            @proba_global_colors.hide
            @proba_per_color_options.show
          end
        end 
      end
      @proba_global_proba = template_proba_global
      flow do
        template_attribute_title('Affected colors : ')
      end
      @proba_global_colors = template_color_global
      @proba_per_color_options = template_proba_per_color
      if @area_preset == 'global'
        @proba_per_color_options.hide
      else
        @proba_global_proba.hide
        @proba_global_colors.hide
      end
    end
  end

  private

  def template_color_global
    stack displace_left: 230, displace_top: -40, margin_right: 200 do
      flow do
        @slim_colors_list = all_colors.map do |name|
          checked = @colors_preset.include?(name.downcase)
          @c = template_tick_box(checked, name)
          [@c, name]
        end
      end
    end
  end

  def template_proba_per_color
    stack displace_left: 230, displace_top: -40 do
      @slim_colors_list_per_color = all_colors.map(&:downcase).map do |name|
        flow do
          if @colors_with_proba_preset.key?(name.downcase.to_sym)
            checked = true
            proba = @colors_with_proba_preset[name.downcase.to_sym]
          else
            checked = false
            proba = '95'
          end
          @c = template_tick_box(checked, name)
          template_paragraph_medium('Probability : ')
          @slim_color_proba = edit_line width: 40
          @slim_color_proba.text = proba
          template_paragraph_small('%')
        end
        [@c, name.downcase, @slim_color_proba]
      end
    end
  end

  def template_proba_global
    flow do
      template_attribute_title('Probability of slim : ')
      @slim_proba = edit_line width: 40
      @slim_proba.text = @probability_preset.to_s
      template_paragraph_small('%')
    end
  end

  def all_colors
    ['White', 'Black', 'Grey', 'Red', 'Blue', 'Green', 'Cyan', 'Yellow',
     'Magenta']
  end

  def all_colors_with_proba
    { "white": "95", "black": "95", "grey": "95", "red": "95", "blue": "95",
      "green": "95", "cyan": "95", "yellow": "95", "magenta": "95" }
  end

  def colors_with_proba_array
    @slim_colors_list_per_color.map do |c, name, proba|
      c.checked? ? [name.downcase.to_sym, proba.text] : []
    end
  end

  def colors_probabilities
    @slim_colors_list_per_color.select{ |c, _, _| c.checked? }.map{ |_, _, proba| proba.text }
  end

  def probability
    @slim_proba.text
  end

  def area
    SanitizerHelper.sanitize_value(@slim_proba_area.text)
  end

  def colors
    @slim_colors_list.map { |c, name| name.downcase.to_sym if c.checked? }.compact
  end

  def direction
    @slim_direction.text.downcase.gsub(' ', '_')
  end

  def colors_with_proba
    Hash[*colors_with_proba_array.flatten]
  end
end
