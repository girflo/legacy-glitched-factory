class TemplateGlitchAlgorithmsDefect < Shoes::Widget
  def library
    'Defect'
  end

  def validator_options
    { iterations: iterations }
  end

  def glitcher_options
    { algorithm: 'defect',
      iterations: iterations.to_i,
      random: random }
  end

  def initialize(presets = {})
    @iterations_preset = presets['iterations'] || 1
    @random_preset = presets['random'] || true
    @random_display_value = para bool_to_value(@random_preset)
    @random = @random_preset
    @random_display_value.hide
    flow do
      flow do
        template_attribute_title('Random : ')
        list_box items: ['No', 'Yes'], choose: bool_to_value(@random_preset) do |random_display_value|
          @random_display_value.text = random_display_value.text
          if random_display_value.text == 'Yes'
            @random = true
            @iterations_div.show
          else
            @random = false
            @iterations_div.hide
          end
        end
      end

      @iterations_div = template_iterations
    end
  end

  private

  def template_iterations
    flow do
      template_attribute_title('Iterarion : ')
      @iterations = edit_line width: 60
      @iterations.text = @iterations_preset.to_s
    end
  end

  def bool_to_value(bool)
    bool ? 'Yes' : 'No'
  end

  def random
    @random
  end

  def iterations
    @iterations.text
  end
end
