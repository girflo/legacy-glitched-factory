class TemplateGlitchAlgorithmsExchange < Shoes::Widget
  def library
    'Exchange'
  end

  def validator_options
    { range: range }
  end

  def glitcher_options
    { algorithm: 'exchange',
      filter: filter,
      range: range.to_i,
      random: random,
      seed: seed }
  end

  def initialize(presets = {})
    @range_preset = presets['range'] || 0
    @random_preset = presets['random'] || false
    @seed_preset = presets['seed'] || 'x'
    @filter_preset = presets['filter'] || 'average'
    @random_exchange = para bool_to_value(@random_preset) 
    @random = @random_preset
    @random_exchange.hide
    flow do
      flow do
        template_attribute_title('Range : ')
        @range = edit_line width: 60
        @range.text = @range_preset.to_s
      end

      flow do
        template_attribute_title('Random exchange : ')
        list_box items: ['No', 'Yes'], choose: bool_to_value(@random_preset) do |random_exchange|
          @random_exchange.text = random_exchange.text
          if random_exchange.text == 'Yes'
            @random = true
            @seed_div.hide
          else
            @random = false
            @seed_div.show
          end
        end
      end
      @seed_div = template_seed
      template_filter
    end
  end

  private

  def bool_to_value(bool)
    bool ? 'Yes' : 'No'
  end

  def template_seed
    flow do
      template_attribute_title('Seed : ')
      @seed = edit_line width: 60
      @seed.text = @seed_preset
    end
  end

  def template_filter
    list_items = %w(None Sub Up Average Paeth Random)
    flow do
      template_attribute_title('Filter to use : ')
      @filter = list_box items: list_items, choose: @filter_preset.capitalize
    end
  end

  def seed
    @seed.text
  end

  def filter
    @filter.text.downcase
  end

  def random
    @random
  end

  def range
    @range.text
  end
end
