class TemplateGlitchAlgorithmsChangeByte < Shoes::Widget
  def library
    'ChangeByte'
  end

  def validator_options
    { byte_numbers: numbers }
  end

  def glitcher_options
    { byte_numbers: numbers }
  end

  def initialize(presets = {})
    @byte_numbers_preset = presets['byte_numbers'] || 0
    flow do
      template_attribute_title('Byte : ')
      @byte_numbers = edit_line width: 40
      @byte_numbers.text = @byte_numbers_preset.to_s
    end
  end

  private

  def numbers
    @byte_numbers.text
  end
end
