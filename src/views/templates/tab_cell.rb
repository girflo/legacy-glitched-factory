require_relative '../../helpers/colors_helper.rb'

class TemplateTabCell < Shoes::Widget
  def initialize(width, content)
    self.width = width
    self.margin = 2
    self.margin_bottom = 14
    background ColorsHelper::MEDIUM_GREY
    border ColorsHelper::MEDIUM_GREY
    @text = para content
    @text.displace_left = 10
    @text.displace_top = 8
    @text.stroke = ColorsHelper::TEXT_LIGHT
  end
end
