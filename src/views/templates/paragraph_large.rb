class TemplateParagraphLarge < Shoes::Widget
  def initialize(text)
    self.width = define_size(text)
    @text = para text
    @text.stroke = ColorsHelper::TEXT_LIGHT
  end

  private

  def define_size(text)
    text == '' ? 0 : 500
  end
end
