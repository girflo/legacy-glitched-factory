# coding: utf-8
class TemplateMetadata < Shoes::Widget
  def initialize(preset = false)
    template_attribute_title('Write metadata : ')
    preset_value = bool_to_value(preset)
    @metadata = preset_value
    list_box items: ['Yes', 'No'], choose: preset_value.capitalize do |value|
      @metadata = value.text.downcase
    end
  end

  def value
    @metadata == 'yes'
  end

  def bool_to_value(bool)
    bool ? 'yes' : 'no'
  end
end
