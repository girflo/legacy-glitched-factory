require_relative '../../helpers/colors_helper.rb'

class TemplateTips < Shoes::Widget
  SCREENS_WITH_TIPS = %w[glitcher profile sorting sampler convert settings].freeze

  def initialize(place = nil)
    return unless display_tips?(place)

    stack do
      background ColorsHelper::GREY
      flow margin_left: 20, margin_right: 20 do
        template_paragraph(CONTENT['tips'][place.to_s])
      end
    end
  end

  private

  def display_tips?(tab)
    (@@settings['tips_visible']) && (SCREENS_WITH_TIPS.include? tab.to_s)
  end
end
