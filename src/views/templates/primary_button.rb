require_relative '../../helpers/colors_helper.rb'

class TemplatePrimaryButton < Shoes::Widget
  def initialize(image_name, action)
    @image_name = image_name
    @action = action
    @content = content_button(ColorsHelper::SECONDARY)
  end

  def success
    @content.clear { content_button(ColorsHelper::GREEN) }
    color_back_to_secondary
  end

  def failure
    @content.clear { content_button(ColorsHelper::RED) }
    color_back_to_secondary
  end

  def color_back_to_secondary
    timer(4) do
      @content.clear { content_button(ColorsHelper::SECONDARY) }
    end
  end

  def content_button(color)
    flow do
      stack width: 200, height: 60 do
        background color
        @link_background = svg "static/images/#{@image_name}",
                               { click: @action }
      end
    end
  end
end
