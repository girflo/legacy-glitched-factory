# coding: utf-8
class TemplateAttributeTitle < Shoes::Widget
  def initialize(content)
    self.width = 230
    self.margin_right = 10
    self.margin_bottom = margin_bottom_size
    @text = para content
    @text.size = element_size
    @text.stroke = ColorsHelper::TEXT_LIGHT
    @text.align = 'right'
  end

  private

  def element_width
    @@settings['compact_interface'] ? 230 : 250
  end

  def element_size
    @@settings['compact_interface'] ? 12 : 13
  end

  def margin_bottom_size
    @@settings['compact_interface'] ? 6 : 14
  end
end
