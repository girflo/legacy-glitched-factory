class AboutScreen < Shoes::Widget
  def call
    template_navigation
    stack do
      background ColorsHelper::GREY
      stack margin_bottom: 20, margin_top: 20 do
        template_subtitle_centered(CONTENT['about']['title'])
        stack margin_left: 20 do
          template_section_subtitle("#{CONTENT['about']['version_text']} #{APPLICATION['app_version']}")
          template_section_subtitle(CONTENT['about']['subtitle'])
          stack margin_left: 20 do
            template_paragraph(CONTENT['about']['content'])
          end
        end
      end
    end
  end
end
