class HelpSamplerScreen < Shoes::Widget
  def call
    template_navigation
    stack do
      background ColorsHelper::GREY
      stack do
        template_subtitle_centered(CONTENT['help']['sampler']['title'])
        stack margin_left: 20 do
          @help_sections = ['goal', 'input', 'output', 'options']
          @help_sections.each do |section|
            template_section_subtitle(CONTENT['help']['sampler'][section.to_s]['title'])
            stack margin_left: 20 do
              template_paragraph(CONTENT['help']['sampler'][section.to_s]['content'])
            end
          end
        end
      end
    end
  end
end
