class GlitcherScreen < Shoes::Widget
  def call
    template_navigation('glitcher')
    template_tips('glitcher')
    @glitch_io = template_glitch_io('glitch')
    template_settings
    @messages = template_messages(CONTENT['error_title'])
    @send_button = template_primary_button('button_glitch.svg', proc { glitch })
  end

  private

  def template_settings
    stack margin_bottom: 20 do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        @profile_position = template_profile
        @algo_selection = template_options
      end
    end
  end

  def template_options
    template_section_title('Options')
    flow margin_left: 20 do
      @metadata = template_metadata
      @options_position = template_options_content({})
    end
  end

  def template_options_content(content)
    @options = template_glitch_algorithms_selection(content)
  end

  def template_profile
    flow do
      template_section_title(CONTENT['profile']['title'])
      template_tips('profile')
      flow margin_left: 20 do
        template_section_subtitle(CONTENT['profile']['create'])
        template_attribute_title(CONTENT['profile']['name'])
        @profile_name = edit_line width: 200
        @profile_name.text = ''
        button CONTENT['profile']['save'], margin_left: 10 do
          save_profile
        end
        all_profiles = ProfileAccessor.all_profiles('profiles_glitcher')
        if !all_profiles.empty?
          template_section_subtitle(CONTENT['profile']['load_profile'])
          template_attribute_title(CONTENT['profile']['name'])
          @profile_to_load = all_profiles.first['name']
          list_box items: all_profiles.map { |profile| profile['name'] } do |profile|
            @profile_to_load = profile.text
          end
          button CONTENT['profile']['load'], margin_left: 10 do
            load_profile(@profile_to_load)
          end
        end
      end
    end
  end

  def validator_global_options
    @glitch_io.params.merge({ algorithm: @options.algorithm })
  end

  def glitcher_global_options
    { glitch_name: @glitch_io.output_filename,
      glitch_folder: @glitch_io.output_folder,
      options: @options.glitcher_options }
  end

  def validate_glitcher
    options = validator_global_options
                .merge(@options.validator_options)
                .merge(validator: @options.library)
    GlitcherValidator.new(options).call
  end

  def validate_profile
    ProfileValidator.new({ profile_name: @profile_name.text,
                           profile_key: 'profiles_glitcher' }).call
  end

  def validate_form(validator)
    @messages.hide_all
    errors = validator
    if errors[:error_numbers].zero?
      @send_button.success
      @messages.show_success(success_message(@glitch_io.input_folder))
      return true
    else
      @send_button.failure
      @messages.show_errors(errors[:error_message])
      return false
    end
  end

  def glitch
    return unless validate_form(validate_glitcher)

    JobsQueue::JobCreator.new(:glitcher, @options.library, @glitch_io.params,
                              @metadata.value, @options.glitcher_options).call
  end

  def save_profile
    return unless validate_form(validate_profile)

    ProfileAccessor.create_new_profile(@profile_name.text,
                                       @options.library,
                                       @metadata.value,
                                       @options.glitcher_options)
    @profile_position.clear { template_profile }
  end

  def load_profile(name)
    content = ProfileAccessor.get_profile_content(name, 'profiles_glitcher')
    @metadata.clear { template_metadata(content['metadata']) }
    @options = nil
    @options_position.clear { template_options_content(content) }
  end

  def success_message(input_folder)
    input_folder.nil? ? 'single_image' : 'multiple_images'
  end
end
