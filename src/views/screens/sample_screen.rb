class SampleScreen < Shoes::Widget
  def call
    template_navigation('sampler')
    template_tips('sampler')
    @sample_io = template_sample_io('_algorithm_filter.png')
    sample_settings
    @messages = template_messages(CONTENT['error_title'])
    @send_button = template_primary_button('button_glitch.svg', proc { sample })
  end

  private

  def sample_settings
    stack margin_bottom: 20 do
      background ColorsHelper::GREY
      stack margin: 8, margin_left: 20 do
        template_section_title(CONTENT['options']['title'])
        @metadata = template_metadata
        @sample_algorithms = template_sample_algorithms
      end
    end
  end

  def sample
    @messages.hide_all
    io_options = { input_file: @sample_io.input_file,
                   output_folder: @sample_io.output_folder }
    errors = SamplerValidator.new(io_options).call
    if errors[:error_numbers].zero?
      @send_button.success
      options = { base_output_filename: @sample_io.output_filename,
                  algorithms_to_sample: @sample_algorithms.algorithms }
      JobsQueue::JobCreator.new(:sampler, 'Sampler', io_options,
                                @metadata.value, options).call
      @messages.show_success('multiple_images')
    else
      @send_button.failure
      @messages.show_errors(errors[:error_message])
    end
  end
end
