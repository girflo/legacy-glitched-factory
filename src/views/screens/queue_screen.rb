class QueueScreen < Shoes::Widget
  def call
    @queue = update_queue
    template_navigation('queue')
    queue_content
  end

  private

  def reload
    stack margin_left: 20, margin_right: 20 do
      template_paragraph_bold("Refresh rate : 60 seconds")
      timer(60) { visit '/queue' }
    end
  end

  def queue_content
    stack do
      background ColorsHelper::GREY
      stack margin_bottom: 20 do
        stack margin_left: 20 do
          template_section_title(CONTENT['queue']['subtitle'], true)
          reload if @@settings['queue_autoreload']
          stack margin_left: 20 do
            button('Refresh') { visit '/queue' }
            queue_table
          end
        end
      end
    end
  end

  def queue_table
    return (template_paragraph(CONTENT['queue']['empty_queue'])) if @queue.empty?

    stack do
      queue_headers
      @queue.each do |item|
        flow do
          formated_description(item)
          formated_kill_link(item)
        end
      end
    end
  end

  def queue_headers
    flow do
      flow width: 200 do
        template_paragraph_bold(CONTENT['queue']['algorithm'])
      end
      flow width: 200 do
        template_paragraph_bold(CONTENT['queue']['type'])
      end
      flow width: 200 do
        template_paragraph_bold(CONTENT['queue']['state'])
      end
    end
  end

  def update_queue
    @@jobs_queue.jobs
  end

  def kill_thread_and_update(job)
    kill_thread(job)
    sleep 0.2
    visit '/queue'
  end

  def kill_thread(job)
    @@jobs_queue.remove_job(job)
  end

  def formated_kill_link(item)
    return if item.running?
    template_tab_cell(60, link('kill', click: proc { kill_thread_and_update(item) }))
  end

  def formated_description(job)
    return if job.nil?

    template_tab_cell(200, job.name.capitalize)
    template_tab_cell(200, job.type.capitalize)
    template_tab_cell(200, job.state.capitalize)
  end
end
