class SortingScreen < Shoes::Widget
  def call
    template_navigation('sorting')
    template_tips('sorting')
    @sorting_io = template_sorting_io('filename.png')
    template_settings
    @messages = template_messages(CONTENT['error_title'])
    @send_button = template_primary_button('button_sort.svg', proc { sort })
  end

  private

  def template_profile
    flow do
      template_section_title(CONTENT['profile']['title'])
      template_tips('profile')
      flow margin_left: 20 do
        template_section_subtitle(CONTENT['profile']['create'])
        template_attribute_title(CONTENT['profile']['name'])
        @profile_name = edit_line width: 200
        @profile_name.text = ''
        button CONTENT['profile']['save'], margin_left: 10 do
          save_profile
        end
        all_profiles = ProfileAccessor.all_profiles('profiles_sorter')
        if !all_profiles.empty?
          template_section_subtitle(CONTENT['profile']['load_profile'])
          template_attribute_title(CONTENT['profile']['name'])
          @profile_to_load = all_profiles.first['name']
          list_box items: all_profiles.map { |profile| profile['name'] } do |profile|
            @profile_to_load = profile.text
          end
          button CONTENT['profile']['load'], margin_left: 10 do
            load_profile(@profile_to_load)
          end
        end
      end
    end
  end

  def template_settings
    stack margin_bottom: 20 do
      background ColorsHelper::GREY
      stack margin_left: 20 do
        @profile_position = template_profile
        template_options
      end
    end
  end

  def template_options
    template_section_title(CONTENT['options']['title'])
    flow margin_left: 20 do
      @metadata = template_metadata
      @options_position = template_option_content({  })
    end
  end

  def template_option_content(content)
    @options = template_sorting_range_sorting(content)
  end

  def io_options
    { input_folder: @sorting_io.input_folder,
      output_folder: @sorting_io.output_folder,
      output_filename: @sorting_io.output_filename }
  end

  def validate_sorter
    SorterValidator.new(io_options).call
  end

  def validate_profile
    ProfileValidator.new({ profile_name: @profile_name.text,
                           profile_key: 'profiles_sorter' }).call
  end

  def validate_form(validator)
    @messages.hide_all
    errors = validator
    if errors[:error_numbers].zero?
      @send_button.success
      @messages.show_success('multiple_images')
      return true
    else
      @send_button.failure
      @messages.show_errors(errors[:error_message])
      return false
    end
  end

  def sort
    return unless validate_form(validate_sorter)

    JobsQueue::JobCreator.new(:sorter, 'Sorter', io_options, @metadata.value,
                              @options.glitcher_options).call
  end

  def save_profile
    return unless validate_form(validate_profile)

    ProfileAccessor.create_new_sorter_profile(@profile_name.text,
                                              @metadata.value,
                                              @options.glitcher_options)
    @profile_position.clear { template_profile }
  end

  def load_profile(name)
    content = ProfileAccessor.get_profile_content(name, 'profiles_sorter')
    @metadata.clear { template_metadata(content['metadata']) }
    @options = nil
    @options_position.clear { @options = template_sorting_range_sorting(content) }
  end
end
