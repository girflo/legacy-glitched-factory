class HelpQueueScreen < Shoes::Widget
  def call
    template_navigation
    stack do
      background ColorsHelper::GREY
      stack do
        template_subtitle_centered(CONTENT['help']['queue']['title'])
        stack margin_left: 20 do
          template_paragraph(CONTENT['help']['queue']['content'])
        end
      end
    end
  end
end
