class ConvertScreen < Shoes::Widget
  def call
    template_navigation('convert')
    template_tips('convert')
    stack margin_bottom: 20 do
      @io = template_convert_io
    end
    @messages = template_messages(CONTENT['error_title'])
    @send_button = template_primary_button('button_glitch.svg', proc { convert })
  end

  private

  def convert
    @messages.hide_all
    errors = ConverterValidator.new(@io.params).call
    if errors[:error_numbers].zero?
      @send_button.success
      JobsQueue::JobCreator.new(:converter, 'Convertion', @io.params,
                                false, {}).call
      @messages.show_success('multiple_images')
    else
      @send_button.failure
      @messages.show_errors(errors[:error_message])
    end
  end
end
