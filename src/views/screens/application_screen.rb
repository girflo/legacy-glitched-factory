require_relative '../application_dependencies.rb'

class ApplicationScreen < Shoes::Widget
  attr_reader :messages

  def initialize(content)
    @content = content
    background ColorsHelper::BACKGROUND
    template_menus
    template_banner
    stack margin_left: 40, margin_right: 40 do
      screen_content
    end
  end

  private

  def screen_content
    @content.call
  end
end
