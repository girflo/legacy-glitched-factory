class HelpProfilesScreen < Shoes::Widget
  def call
    template_navigation
    stack do
      background ColorsHelper::GREY
      stack do
        template_subtitle_centered(CONTENT['help']['profiles']['title'])
        stack margin_left: 20 do
          template_paragraph(CONTENT['help']['profiles']['content'])
        end
      end
    end
  end
end
