class SettingsScreen < Shoes::Widget
  def call
    template_navigation
    all_settings
    @messages = template_messages(CONTENT['error_title'])
    @save_button = template_primary_button('button_save.svg', proc { save })
  end

  private

  def all_settings
    stack margin_bottom: 20 do
      background ColorsHelper::GREY
      stack do
        template_subtitle_centered(CONTENT['settings']['title'])
        template_tips('settings')
        stack margin_left: 20 do
          @tips_visibility = template_settings_tips_visibility
          @queue_autoreload = template_settings_queue_autoreload
          @compact_interface = template_settings_compact_interface
          @algorithm_in_name = template_settings_algorithm_in_name
          template_section_subtitle(CONTENT['settings']['default_path'])
          @default_input = template_settings_default_input
          @default_input_sampler = template_settings_default_input_sampler
          @default_input_sorter = template_settings_default_input_sorter
          @default_output = template_settings_default_output
          template_section_subtitle(CONTENT['profiles_export']['title'])
          flow do
            template_attribute_title(CONTENT['profiles_export']['selected_output_directory'])
            button CONTENT['profiles_export']['output_directory'] do
              @output_folder = ask_open_folder
              @choosed_output.clear do
                template_attribute_title(CONTENT['profiles_export']['selected_output_directory'])
                template_paragraph_large("#{@output_folder}")
              end
            end
          end
          @choosed_output = stack {}
          flow do
            button CONTENT['profiles_export']['export_all'] do
              export
            end
          end
          template_section_subtitle(CONTENT['profiles_import']['title'])
          flow margin_bottom: 10 do
            template_attribute_title(CONTENT['profiles_import']['selected_json'])
            button CONTENT['profiles_import']['profile_file'] do
              @import_file = ask_open_file title: CONTENT['profiles_import']['choose_profile_file']
              @choosed_import_file.clear do
                template_attribute_title(CONTENT['profiles_import']['selected_profile_file'])
                template_paragraph("#{@import_file}")
              end
            end
          end
          @choosed_import_file = stack {}
          flow margin_bottom: 10 do
            button CONTENT['profiles_import']['import_profiles'] do
              import
            end
          end
        end
      end
    end
  end

  def validate_profile_exporter
    output_folder = @output_folder || ''
    ProfileExporterValidator.new({ output_folder: output_folder }).call
  end

  def validate_profile_importer
    input_file = @import_file || ''
    ProfileImporterValidator.new({ input_file: input_file }).call
  end

  def validate_form(validator, success_message)
    @messages.hide_all
    errors = validator
    if errors[:error_numbers].zero?
      @messages.show_success(success_message)
      return true
    else
      @messages.show_errors(errors[:error_message])
      return false
    end
  end

  def export
    return unless validate_form(validate_profile_exporter, 'profiles_export')

    profiles_file = 'static/profile.json'
    dest_folder = @output_folder
    FileUtils.cp(profiles_file, dest_folder)
  end

  def import
    return unless validate_form(validate_profile_importer, 'profiles_import')

    ProfileAccessor.import_profiles_from_file(@import_file)
  end

  def save
    @messages.hide_all
    SettingUpdater.new({ tips_visibility: @tips_visibility.setting,
                         queue_autoreload: @queue_autoreload.setting,
                         compact_interface: @compact_interface.setting,
                         algorithm_in_name: @algorithm_in_name.setting,
                         default_input: @default_input.input,
                         default_input_type: @default_input.type,
                         default_output: @default_output.output,
                         default_input_sampler: @default_input_sampler.input,
                         default_input_sorter: @default_input_sorter.input }).call
    @@settings = YAML.load_file('settings.yml')
    @save_button.success
    @messages.show_success('settings')
    timer(3) { visit '/settings' }
  end
end
