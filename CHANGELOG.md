# CHANGELOG
## Version 0.29
  - Add defect algorithm

## Version 0.29
  - Add converter for jpg images
  - Rename the project Glitched Factory
  - Improve handling of output filename

## Version 0.28
  - Add default values in settings
  - Improve message display
  - Animate buttons
  - Add timestamp when image already exists

## Version 0.27
  - Create tips
  - Create settings
  - Add possibility to write metadata
  - Automatically reload the queue
  - Create profiles
  - Improve help

## Version 0.26
  - Fix pixel sorter second range
  - Create range pixel sorting

## Version 0.25
  - Create dark theme
  - Fix issue with brush

## Version 0.24
  - Add queue

## Version 0.23
  - Add image preview
  - Add children number for input directory
  - Add success notifications
  - Complete the help section

## Version 0.22
  - Canceling the input selection does not erase the current selected one
  - New interface

## Version 0.21
  - Create pixel sorter
  
## Version 0.20
  - Improve general UI
  - Create menus
  - Add success

## Version 0.19
  - Add Wrong filter algorithm
  - Add Change byte algorithm
  - Add algorithms section to the sampler

## Version 0.18
  - Add custom name to sampler output
  - Open the app in full screen
  - Add Brush algorithm

## Version 0.17
  - Rename the project "Glitch Factory"
  - Add right to left and down to up directions to Slim
  - Add probability per color to Slim
  
## Version 0.16
  - Add Slim algorithm
  - Add new banner
  
## Version 0.15
  - Create about page
  - Create footer for about and help links
  - Hide and show exchange options according to the selected algorithm
  - Add seed for transpose algorithm
  - Various design improvement

## Version 0.14
  - Add support for random glitching
  - Add display of the input source
  - When selecting input image the input directory is remove
  - When selecting input directory the input image is remove
  - Add banner with logo

## Version 0.13
  - Creation of the changelog
  - Creation of the help page
  - Creation of the sampler
