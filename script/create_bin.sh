#!/bin/sh
create_bin () {
    mkdir dmg
    cd dmg
    ruby ../utils/dmg_packager_osx.rb ../utils/dmg_info_osx.yml
    cd ..
    libName='GlitchedFactory-osx-'
    libExtension='.dmg'
    cp dmg/GlitchedFactory-Installer.dmg ../../glitched_factory_website/bin/"$libName$1$libExtension"
    mv dmg/GlitchedFactory-Installer.dmg ../bin/"$libName$1$libExtension"
    sudo rm -r dmg
}

if [[ -z "$1" ]]; then
    create_bin ""
else
    create_bin $1
fi
