#!/bin/sh
create_tag () {
    git checkout master
    git merge develop
    vim ../CHANGELOG.md
    ruby utils/update_version.rb -v $1
    tagName='v'
    tagMessage='version '
    commitMessage='Version '
    ./create_bin.sh $1
    git add ../bin/*
    git add ../CHANGELOG.md
    git add ../src/application.yml
    git add utils/dmg_info_osx.yml
    git commit -m "$commitMessage$1"
    git tag -a "$tagName$1" -m "$tagMessage$1"
    git push --follow-tags
}

if [[ -z "$1" ]]; then
    echo "Missing version number"
    echo "Usage : ./new_version.sh <version number>"
else
    create_tag $1
fi
