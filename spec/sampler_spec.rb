require_relative '../src/lib/glitchers/sampler.rb'

RSpec.describe Sampler do
  let(:io_params) { { input_file: 'spec/input_folder/cave.png',
                      output_folder: 'spec/output_folder' } }
  let(:metadata) { false }
  let(:options) { { algorithms_to_sample: algorithms,
                    base_output_filename: 'sample' } }
  let(:io) { Io.new(io_params) }
  
  describe 'brush' do
    before do
      Glitcher.class_variable_set(:@@settings, {})
      Sampler.new(io, metadata, options).call
    end

    after do
      File.delete(filename1) if File.exist? filename1
      File.delete(filename2) if File.exist? filename2
      File.delete(filename3) if File.exist? filename3
      File.delete(filename4) if File.exist? filename4
    end

    let(:algorithms) { ['brush'] }
    let(:filename1) { 'spec/output_folder/sample_brush_horizontal.png' }
    let(:filename2) { 'spec/output_folder/sample_brush_horizontal_inverted.png' }
    let(:filename3) { 'spec/output_folder/sample_brush_vertical.png' }
    let(:filename4) { 'spec/output_folder/sample_brush_vertical_inverted.png' }

    it { expect(File.file?(filename1)).to be true }
    it { expect(File.file?(filename2)).to be true }
    it { expect(File.file?(filename3)).to be true }
    it { expect(File.file?(filename4)).to be true }
  end

  describe 'exchange' do
    before do
      Sampler.new(io, metadata, options).call
    end

    after do
      File.delete(filename1) if File.exist? filename1
      File.delete(filename2) if File.exist? filename2
      File.delete(filename3) if File.exist? filename3
      File.delete(filename4) if File.exist? filename4
      File.delete(filename5) if File.exist? filename5
    end

    let(:algorithms) { ['exchange'] }
    let(:filename1) { 'spec/output_folder/sample_exchange_average.png' }
    let(:filename2) { 'spec/output_folder/sample_exchange_none.png' }
    let(:filename3) { 'spec/output_folder/sample_exchange_paeth.png' }
    let(:filename4) { 'spec/output_folder/sample_exchange_sub.png' }
    let(:filename5) { 'spec/output_folder/sample_exchange_up.png' }

    it { expect(File.file?(filename1)).to be true }
    it { expect(File.file?(filename2)).to be true }
    it { expect(File.file?(filename3)).to be true }
    it { expect(File.file?(filename4)).to be true }
    it { expect(File.file?(filename5)).to be true }
  end

  describe 'change byte' do
    before do
      Sampler.new(io, metadata, options).call
    end

    after do
      File.delete(filename1) if File.exist? filename1
    end

    let(:algorithms) { ['change_byte'] }
    let(:filename1) { 'spec/output_folder/sample_change_byte.png' }

    it { expect(File.file?(filename1)).to be true }
  end

  describe 'slim' do
    before do
      Sampler.new(io, metadata, options).call
    end

    after do
      File.delete(filename1) if File.exist? filename1
      File.delete(filename2) if File.exist? filename2
      File.delete(filename3) if File.exist? filename3
      File.delete(filename4) if File.exist? filename4
    end

    let(:algorithms) { ['slim'] }
    let(:filename1) { 'spec/output_folder/sample_slim_down_to_up.png' }
    let(:filename2) { 'spec/output_folder/sample_slim_left_to_right.png' }
    let(:filename3) { 'spec/output_folder/sample_slim_right_to_left.png' }
    let(:filename4) { 'spec/output_folder/sample_slim_up_to_down.png' }

    it { expect(File.file?(filename1)).to be true }
    it { expect(File.file?(filename2)).to be true }
    it { expect(File.file?(filename3)).to be true }
    it { expect(File.file?(filename4)).to be true }
  end

  describe 'transpose' do
    before do
      Sampler.new(io, metadata, options).call
    end

    after do
      File.delete(filename1) if File.exist? filename1
      File.delete(filename2) if File.exist? filename2
      File.delete(filename3) if File.exist? filename3
      File.delete(filename4) if File.exist? filename4
      File.delete(filename5) if File.exist? filename5
    end

    let(:algorithms) { ['transpose'] }
    let(:filename1) { 'spec/output_folder/sample_transpose_average.png' }
    let(:filename2) { 'spec/output_folder/sample_transpose_none.png' }
    let(:filename3) { 'spec/output_folder/sample_transpose_paeth.png' }
    let(:filename4) { 'spec/output_folder/sample_transpose_sub.png' }
    let(:filename5) { 'spec/output_folder/sample_transpose_up.png' }

    it { expect(File.file?(filename1)).to be true }
  end

  describe 'wrong_filter' do
    before do
      Sampler.new(io, metadata, options).call
    end

    after do
      File.delete(filename1) if File.exist? filename1
    end

    let(:algorithms) { ['wrong_filter'] }
    let(:filename1) { 'spec/output_folder/wrong_filter.png' }
  end
end
