require_relative '../src/helpers/sanitizer_helper.rb'

RSpec.describe SanitizerHelper do
  describe '.unsanitize_value' do
    subject { SanitizerHelper.unsanitize_value(value) }
    let(:value) { 'sanitized_value' }
    it { is_expected.to eq('Sanitized value') }
  end

  describe '.sanitize_value' do
    subject { SanitizerHelper.sanitize_value(value) }
    let(:value) { 'Unanitized Value' }
    it { is_expected.to eq('unanitized_value') }
  end

  describe '.bool_to_yes_no' do
    subject { SanitizerHelper.bool_to_yes_no(value) }

    context 'when true' do
      let(:value) { true }
      it { is_expected.to eq('Yes') }
    end

    context 'when false' do
      let(:value) { false }
      it { is_expected.to eq('No') }
    end
  end
end
