require_relative '../src/lib/glitchers/glitcher.rb'

RSpec.describe Glitcher do
  #let(:glitcher) { GlitcherAsync.new(glitch_library, io_params, metadata, options) }
  let(:io_params) { { input_file: 'spec/input_folder/cave.png', input_path: nil,
                      output_filename: output_filename,
                      output_folder: 'spec/output_folder' } }
  let(:metadata) { false }
  let(:options) { {} }

  before(:each) do
    io = Io.new(io_params)
    Glitcher.class_variable_set(:@@settings, {})
    Glitcher.new(glitch_library, io, false, metadata, options).call
  end

  after(:each) do
    File.delete(filename) if File.exist? filename
  end

  describe 'brush' do
    let(:glitch_library) { 'Brush' }
    let(:output_filename) { 'brush' }
    let(:filename) { 'spec/output_folder/brush.png' }

    it { expect(File.file?(filename)).to be true }
  end

  describe 'pixel exchange' do
    let(:glitch_library) { 'Exchange' }
    let(:output_filename) { 'pixel_exchange' }
    let(:filename) { 'spec/output_folder/pixel_exchange.png' }

    it { expect(File.file?(filename)).to be true }
  end

  describe 'change byte' do
    let(:glitch_library) { 'ChangeByte' }
    let(:output_filename) { 'change_byte' }
    let(:filename) { 'spec/output_folder/change_byte.png' }

    it { expect(File.file?(filename)).to be true }
  end

  describe 'pixel sorting' do
    let(:glitch_library) { 'PixelSorter' }
    let(:output_filename) { 'pixel_sorting' }
    let(:filename) { 'spec/output_folder/pixel_sorting.png' }

    it { expect(File.file?(filename)).to be true }
  end

  describe 'slim' do
    let(:glitch_library) { 'Slim' }
    let(:output_filename) { 'slim' }
    let(:filename) { 'spec/output_folder/slim.png' }

    it { expect(File.file?(filename)).to be true }
  end

  describe 'transpose' do
    let(:glitch_library) { 'Transpose' }
    let(:output_filename) { 'transpose' }
    let(:filename) { 'spec/output_folder/transpose.png' }

    it { expect(File.file?(filename)).to be true }
  end

  describe 'wrong_filter' do
    let(:glitch_library) { 'WrongFilter' }
    let(:output_filename) { 'wrong_filter' }
    let(:filename) { 'spec/output_folder/wrong_filter.png' }

    it { expect(File.file?(filename)).to be true }
  end
end
