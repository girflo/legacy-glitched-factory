require_relative '../src/lib/io.rb'

RSpec.describe Io do
  describe '.image' do
    subject { Io.new(attributes).images }
    context 'when input is image' do
      context 'when input info is specified' do
        let(:attributes) { { input_file: 'cave.png', input_path: nil,
                             output_folder: 'spec/output_folder'} }
        it { is_expected.to eq([{ input_filename: 'cave.png',
                                  output_filename: 'spec/output_folder/glitch.png' }]) }
      end

      context 'when output info are specified' do
        let(:attributes) { { input_file: 'cave.png', input_path: nil,
                             output_folder: 'spec/output_folder',
                             output_filename: 'new_output_name' } }
        it { is_expected.to eq([{ input_filename: 'cave.png',
                                  output_filename: 'spec/output_folder/new_output_name.png' }]) }
      end
    end

    context 'when input is directory' do
      context 'when input info is specified' do
        let(:attributes) { { input_file: nil, input_folder: 'spec/input_folder',
                             output_folder: 'spec/output_folder'} }
        it { is_expected.to eq([{ input_filename: 'spec/input_folder/cave.png',
                                  output_filename: 'spec/output_folder/glitchcave.png' },
                                { input_filename: 'spec/input_folder/secondcave.png',
                                  output_filename: 'spec/output_folder/glitchsecondcave.png' }]) }
      end

      context 'when output info are specified' do
        let(:attributes) { { input_file: nil, input_folder: 'spec/input_folder',
                             output_folder: 'spec/output_folder',
                             output_filename: 'new_output_name' } }
        it { is_expected.to eq([{ input_filename: 'spec/input_folder/cave.png',
                                  output_filename: 'spec/output_folder/new_output_namecave.png' },
                                { input_filename: 'spec/input_folder/secondcave.png',
                                  output_filename: 'spec/output_folder/new_output_namesecondcave.png' }]) }
      end
    end
  end

  describe '.define_output_filename' do
    let(:io) { Io.new(attributes) }
    let(:timestamp) { Time.now.getutc.strftime "%Y%m%d_%H%M%S" }
    subject { io.define_output_filename }

    context 'when the output file already exists' do
      before do
        allow(io).to receive(:time_utc_string).and_return(timestamp)
      end

      let(:attributes) { { input_file: 'cave.png', input_path: nil,
                           output_folder: 'spec/input_folder',
                           output_filename: 'cave' } }
      it { is_expected.to eq("spec/input_folder/#{timestamp}_cave.png") }
    end
  end

  describe '.define_output_filename_in_directory' do
    let(:io) { Io.new(attributes) }
    let(:timestamp) { Time.now.getutc.strftime "%Y%m%d_%H%M%S" }
    let(:img) { 'cave.png' }
    subject { io.define_output_filename_in_directory(img) }

    context 'when the output file already exists' do
      before do
        allow(io).to receive(:time_utc_string).and_return(timestamp)
      end

      let(:attributes) { { input_file: nil, input_path: 'spec/input_folder',
                           output_folder: 'spec/input_folder',
                           output_filename: 'second' } }
      it { is_expected.to eq("spec/input_folder/second#{timestamp}_cave.png") }
    end
  end
end
