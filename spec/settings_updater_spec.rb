require_relative '../src/lib/settings_updater.rb'
require 'yaml'

RSpec.describe SettingUpdater do
  before(:all) do
    FileUtils.cp('src/settings.yml', 'spec/settings.yml')
  end

  after(:all) do
    FileUtils.rm ['spec/settings.yml']
  end

  describe 'default configuration' do
    let(:updater) { SettingUpdater.new(false, false) }
    let(:content) { YAML.safe_load(File.open('spec/settings.yml').read) }

    context 'tips visibility' do
      subject { content['tips_visible'] }
      it { is_expected.to be true }
    end

    context 'queue autoreload' do
      subject { content['queue_autoreload'] }
      it { is_expected.to be true }
    end

    context 'conpact interface' do
      subject { content['compact_interface'] }
      it { is_expected.to be true }
    end

    context 'library in name' do
      subject { content['algorithm_in_name'] }
      it { is_expected.to be false }
    end

    context 'default input' do
      subject { content['default_input'] }
      it { is_expected.to eq '' }
    end

    context 'default input' do
      subject { content['default_input_type'] }
      it { is_expected.to eq '' }
    end

    context 'default output' do
      subject { content['default_output'] }
      it { is_expected.to eq '' }
    end

    context 'default input sampler' do
      subject { content['default_input_sampler'] }
      it { is_expected.to eq '' }
    end

    context 'default input sampler' do
      subject { content['default_input_sorter'] }
      it { is_expected.to eq '' }
    end
  end

  describe '.call' do
    before(:each) do
      FileUtils.cp(input_yml_file, 'spec/settings_current.yml')
      allow(updater).to receive(:settings_file_location).and_return('spec/settings_current.yml')
      updater.call
    end

    after(:each) do
      File.delete('spec/settings_current.yml') if File.exist? 'spec/settings_current.yml'
    end
    let(:new_settings_content) { YAML.load(File.open('spec/settings_current.yml').read) }

    context 'when settings changed from true to false' do
      let(:input_yml_file) { 'spec/settings.yml' }
      let(:updater) { SettingUpdater.new({ tips_visibility: false, queue_autoreload: false}) }
      it { expect(new_settings_content['tips_visible']).to be false }
      it { expect(new_settings_content['queue_autoreload']).to be false }
    end

    context 'when compact_interface changed from true to false' do
      let(:input_yml_file) { 'spec/settings.yml' }
      let(:updater) { SettingUpdater.new({ compact_interface: true }) }
      it { expect(new_settings_content['compact_interface']).to be true }
    end

    context 'when algorithm_in_name changed from true to false' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) { SettingUpdater.new({ algorithm_in_name: true }) }
      it { expect(new_settings_content['algorithm_in_name']).to be true }
    end

    context 'when settings changed from false to true' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) do
        SettingUpdater.new({ tips_visibility: true, queue_autoreload: true })
      end
      it { expect(new_settings_content['tips_visible']).to be true }
      it { expect(new_settings_content['queue_autoreload']).to be true }
    end

    context 'when compact_interface changed from false to true' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) { SettingUpdater.new({ compact_interface: true }) }
      it { expect(new_settings_content['compact_interface']).to be true }
    end

    context 'when algorithm_in_name changed from false to true' do
      let(:input_yml_file) { 'spec/settings.yml' }
      let(:updater) { SettingUpdater.new({ algorithm_in_name: true }) }
      it { expect(new_settings_content['algorithm_in_name']).to be true }
    end

    context 'when default input is set as directory' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) do
        SettingUpdater.new({ default_input: 'spec/input_folder',
                             default_input_type: 'directory' })
      end
      it { expect(new_settings_content['default_input']).to eq 'spec/input_folder' }
      it { expect(new_settings_content['default_input_type']).to eq 'directory' }
    end

    context 'when default input is set as directory' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) do
        SettingUpdater.new({ default_input: 'spec/input_folder/cave.png',
                             default_input_type: 'file' })
      end
      it { expect(new_settings_content['default_input']).to eq 'spec/input_folder/cave.png' }
      it { expect(new_settings_content['default_input_type']).to eq 'file' }
    end

    context 'when default output is set' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) { SettingUpdater.new({ default_output: 'spec/output_folder' }) }
      it { expect(new_settings_content['default_output']).to eq 'spec/output_folder' }
    end

    context 'when default input sampler is set' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) do
        SettingUpdater.new({ default_input_sampler: 'spec/input_folder/cave.png' })
      end
      it { expect(new_settings_content['default_input_sampler']).to eq 'spec/input_folder/cave.png' }
    end

    context 'when default input sorter is set' do
      let(:input_yml_file) { 'spec/settings_false.yml' }
      let(:updater) do
        SettingUpdater.new({ default_input_sorter: 'spec/input_folder/' })
      end
      it { expect(new_settings_content['default_input_sorter']).to eq 'spec/input_folder/' }
    end
  end
end
